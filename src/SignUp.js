import { Text, View, StyleSheet, Image, TextInput, Alert, TouchableOpacity, ScrollView } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { Component } from 'react'
import { star, openEye, closeEye, on } from './assets'
import Scale from './Scale'
export default class SignUp extends Component {
    constructor() {
        super()
        this.state = {
            eyeControl: false,
            circleTick: false,
            userName: "",
            userNameError: "",
            userNamestatus: false,
            email: '',
            emailError: '',
            emailstatus: false,
            password: '',
            passwordError: '',
            passwordStatus: false,
        }
    }

    renderHeader = () => {
        return (
            <View style={styles.header}>
                <View></View>
                <Image source={star} style={styles.starstyle} resizeMode='cover' />
            </View>
        )
    }

    rendercreateAccountHeading = () => {
        return (
            <View style={styles.accountHeading}>
                <Text style={styles.createAccountStyle}>Create Account</Text>
            </View>
        )
    }

    renderForm1 = () => {
        return (
            <View>
                <View style={styles.formMain}>
                    <Text style={styles.userNameText}>Username</Text>
                    <View style={styles.textInputElement}>
                        <TextInput style={styles.textInputStyle} value={this.state.userName} onChangeText={(text) => this.handleUserName(text)} placeholder='Rajeev Desai' />
                    </View>
                    <Text style={styles.errortext}>{this.state.userNameError}</Text>
                </View>

                <View style={styles.formMain}>
                    <Text style={styles.userNameText}>Email</Text>
                    <View style={styles.textInputElement}>
                        <TextInput style={styles.textInputStyle} autoComplete='off' value={this.state.email} onChangeText={(text) => this.handleEmail(text)} placeholder='abcd@gmail.com' />
                    </View>
                    <Text style={styles.errortext}>{this.state.emailError}</Text>
                </View>

                <View style={styles.formMain}>
                    <Text style={styles.userNameText}>Password</Text>
                    <View style={styles.textInputElement}>
                        <TextInput style={styles.textInputStyle} value={this.state.password} onChangeText={(text) => this.handlePassword(text)} secureTextEntry={!this.state.eyeControl} placeholder='Xyz@name123' />
                        <TouchableOpacity onPress={this.handleEyeControl}><Image source={this.state.eyeControl ? openEye : closeEye} style={styles.openEyeStyle} resizeMode='cover' /></TouchableOpacity>
                    </View>
                    <Text style={styles.errortext}>{this.state.passwordError}</Text>
                </View>
            </View>
        )
    }
    handleUserName = (gettext) => {
        const regEx = /^[a-zA-Z]+ [a-zA-Z]+$/;
        if (gettext.length <= 1) {
            this.setState({ userNameError: "", userName: gettext, userNamestatus: false })
        }
        if (regEx.test(this.state.userName)) {
            this.setState({ userNamestatus: true, userName: gettext })
            // this.setState({userNameError:"matched"})
        } else {
            this.setState({ userNamestatus: false, userName: gettext })
            // this.setState({userNameError:"not matched"})

        }
    }

    handleEmail = (gettext) => {
        const regEx = /[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,8}(.[a-z{2,8}])?/g;
        if (gettext.length <= 1) {
            this.setState({ emailError: "", email: gettext, emailstatus: false })
        }
        if (regEx.test(this.state.email)) {
            this.setState({ emailstatus: true, email: gettext })
        } else {
            this.setState({ emailstatus: false, email: gettext })
        }
    }

    handlePassword = (gettext) => {
        const regEx = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/
        if (gettext.length <= 1) {
            this.setState({ passwordError: "", password: gettext, password: false })
        }
        if (regEx.test(this.state.password)) {
            this.setState({ passwordStatus: true, password: gettext })
        } else {
            this.setState({ passwordStatus: false, password: gettext })
        }
    }
    handleEyeControl = () => {
        // if (this.state.eyeControl === false) {
        //     this.setState({ eyeControl: true })
        // } else {
        //     this.setState({ eyeControl: false })
        // }
        { this.state.eyeControl ? this.setState({ eyeControl: false }) : this.setState({ eyeControl: true }) }
    }

    termsAndCondition = () => {
        return (
            <View style={styles.termsConditionMain}>
                {this.state.circleTick ?
                    <TouchableOpacity onPress={this.toggleTick}><Image source={on} style={styles.circle} /></TouchableOpacity>
                    :
                    <TouchableOpacity onPress={this.toggleTick}><View style={styles.circle}>
                    </View>
                    </TouchableOpacity>}

                <Text>I accept the terms and privacy policy</Text>
            </View>
        )
    }
    toggleTick = () => {
        // if (this.state.circleTick === false) {
        //     this.setState({ circleTick: true })
        // } else {
        //     this.setState({ circleTick: false })
        // }
        { this.state.circleTick ? this.setState({ circleTick: false }) : this.setState({ circleTick: true }) }
    }
    loginButton = () => {
        return (
            <View style={styles.loginButtonMainView}>
                <TouchableOpacity onPress={this.validateForm} style={styles.loginButtonStyle}>
                    <Text style={styles.logInText}>Log In</Text>
                </TouchableOpacity>
            </View>
        )
    }
    validateForm = async () => {
        if (this.state.userNamestatus == false) {
            this.setState({ userNameError: 'Username Should be in Proper Format', emailError: '', passwordError: '' })
            return
        }
        if (this.state.emailstatus == false) {
            this.setState({ emailError: 'email should be proper format', userNameError: '', passwordError: '' })
            return
        }
        if (this.state.passwordStatus == false) {
            this.setState({ passwordError: 'Password should be in proper Format', userNameError: '', emailError: '' })
            return
        }
        if (this.state.circleTick == false) {
            Alert.alert('Check Box Alert', 'Please Check on Terms and Conditions', [{
                text: 'OK',
                style: 'OK',
            }])
        } else {
            let arrOfEmails;
            let customerArray;
            let getDesaireEmail;
            let customer;
            let stateofListofEmails = await AsyncStorage.getItem("listOfEmails")
            let stateofListofEmailsParse = JSON.parse(stateofListofEmails)

            if (stateofListofEmails == null) {

                customer = [{
                    customerName: this.state.userName,
                    customerEmail: this.state.email,
                    customerPassword: this.state.password,
                }]
                arrOfEmails = [this.state.email]
                await AsyncStorage.setItem("listOfEmails", JSON.stringify(arrOfEmails));
                customerArray = [...customer];
                await AsyncStorage.setItem("customerDetails", JSON.stringify(customerArray))
                console.log("*************EMPTY WALA********************", customerArray)
                this.setState({ userName: "", email: "", password: "" })
            } 
            
            getDesaireEmail = stateofListofEmailsParse.find((listArr) => {
                return listArr == this.state.email
            });
            if (getDesaireEmail) {
                this.setState({ emailError: "Email is already registerd" })
                return;
            } else {
                customer = [{
                    customerName: this.state.userName,
                    customerEmail: this.state.email,
                    customerPassword: this.state.password,
                }]
                let TempArray = await AsyncStorage.getItem('customerDetails')
                let TempArrayParse = JSON.parse(TempArray)
                customerArray = [...TempArrayParse, ...customer]
                await AsyncStorage.setItem("customerDetails", JSON.stringify(customerArray))
                console.log("!!!!!!!!!!!!!!!ALL LIST!!!!!!!!!!!!!!!!!!!!", customerArray)
                let getemailList = await AsyncStorage.getItem("listOfEmails");
                let getemailListParse = JSON.parse(getemailList)
                arrOfEmails = [...getemailListParse, this.state.email]
                console.log("^^^^^^^^^^^^^^^array LIst^^^^^^^^^^^^^^^^^^^", arrOfEmails)
                await AsyncStorage.setItem("listOfEmails", JSON.stringify(arrOfEmails))
                Alert.alert(`User Name= ${this.state.userName}`, `User Email = ${this.state.email}`, [{
                    text: 'Cancel',
                    onPress: () => this.setState({ userNameError: '', emailError: '', passwordError: '' }),
                    style: 'cancel',
                }, {
                    text: 'Ok',
                    onPress: () => {
                        this.setState({ userNameError: '', emailError: '', passwordError: '' })
                    },
                    style: 'OK',
                }
                ])
                this.setState({ userName: "", email: "", password: "" })

                return
            }
        }
    }

    alreadyHaveAccount = () => {
        return (

            <View style={styles.alreadyHaveAccountMain}>
                <Text style={styles.haveAccount}>Already have an account ?</Text>
                <View style={styles.justUnderline}>
                    <Text style={styles.haveAccountLogin}>Log in</Text>
                </View>
            </View>

        )
    }
    render() {
        return (
            <ScrollView style={{ flex: 1, paddingHorizontal: Scale(10), paddingVertical: Scale(10), backgroundColor: '#fff' }}>
                <View style={{ backgroundColor: '#F0F3FB', borderRadius: 20, paddingHorizontal: Scale(10) }}>
                    {this.renderHeader()}
                    {this.rendercreateAccountHeading()}
                    {this.renderForm1()}
                    {this.termsAndCondition()}
                    {this.loginButton()}
                    {this.alreadyHaveAccount()}
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    starstyle: {
        width: Scale(46),
        height: Scale(44),
    },
    accountHeading: {
        marginTop: 54,
    },
    createAccountStyle: {
        fontSize: 30,
        fontWeight: 'bold',
    },
    textInputElement: {
        width: '100%',
        height: Scale(56),
        borderWidth: 1,
        paddingHorizontal: Scale(16),
        paddingVertical: Scale(18),
        borderColor: '#D8DADC',
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#fff'
    },
    textInputStyle: {
        width: 250,

    },
    userNameText: {
        marginBottom: 6,
        fontSize: 14,
        fontWeight: 'normal'
    },
    formMain: {
        marginTop: Scale(38)
    },
    openEyeStyle: {
        marginTop: Scale(3),
        marginRight: Scale(2),
    },
    circle: {
        width: 20,
        height: 20,
        borderRadius: 50,
        borderWidth: 1,
        marginRight: Scale(12),
        backgroundColor: '#fff',
    },
    termsConditionMain: {
        flexDirection: 'row',
        marginTop: Scale(24),
    },
    loginButtonStyle: {
        width: '100%',
        height: Scale(56),
        backgroundColor: 'black',
        marginTop: Scale(39),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    logInText: {
        fontSize: Scale(16),
        fontWeight: "900",
        color: 'white',
    },
    loginButtonMainView: {
        alignItems: 'center',
    },
    errortext: {
        fontSize: 10,
        fontWeight: '500',
        color: 'red',
    },
    haveAccount: {
        fontSize: 14,
        fontWeight: '500',
        color: '#585858',
        marginRight: 10
    },
    haveAccountLogin: {
        fontSize: 14,
        fontWeight: 'bold',
    },
    justUnderline: {
        borderBottomWidth: 2,
    },
    alreadyHaveAccountMain: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        flex: 1,
        justifyContent: 'center',
        marginTop: Scale(10),
    }
}) 