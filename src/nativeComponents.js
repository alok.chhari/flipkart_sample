import React, { Component } from 'react';
import { StatusBar } from 'expo-status-bar';
import { View, Text, Switch, TouchableHighlight, TouchableNativeFeedback, Pressable, TouchableOpacity, ImageBackground, Modal, Button } from 'react-native';
import { addImage } from './assets';
import * as ImagePicker from 'expo-image-picker';
import DateTimePicker from '@react-native-community/datetimepicker'
import * as DocumentPicker from 'expo-document-picker';
import * as ExpoFileSystem from "expo-file-system";
export default class MyComponent extends Component {
    constructor() {
        super()
        this.state = {
            switchValue: false,
            isModalVisible: false,
            getImage: false,
            date: new Date(),
            showPicker: false,
            getDate: '',
            docName:'',
        }
    }
    // toggleSwitch=(value)=>{
    //  this.setState({switchValue:value})
    // }
    renderMainBody = () => {
        return (

            <View style={{ flex: 1, backgroundColor: 'orange' }}>
                <StatusBar
                    backgroundColor='pink'
                    hidden={false}
                    translucent={true}
                />
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <Switch
                            trackColor={{ false: 'lightblue', true: 'pink' }}
                            thumbColor={this.state.switchValue ? "brown" : "green"}
                            value={this.state.switchValue}
                            // onValueChange={this.toggleSwitch}
                            onValueChange={(value) => this.setState({ switchValue: value })}
                        />
                        <Text>Switch {this.state.switchValue ? "On" : "Off"}</Text>
                    </View>
                    <TouchableHighlight
                        activeOpacity={1}
                        underlayColor="lightblue"
                        onPress={() => alert('You Clicked Me')}
                        style={{ padding: 10, borderWidth: 2, width: 250, backgroundColor: 'pink', borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Text style={{ fontSize: 18, fontWeight: "600" }}>TouchableHighlight</Text>
                    </TouchableHighlight>
                    <TouchableOpacity onPress={() => this.setState({ isModalVisible: true })} style={{ width: 100, height: 100, borderRadius: 60 }}>
                        <ImageBackground source={this.state.getImage ?{uri:`${this.state.getImage}`}:require('../assets/addImage.png')}  imageStyle={{ borderRadius: 60 }} style={{ width: 100, height: 100, borderRadius: 60 }} />
                    </TouchableOpacity>
                    <Pressable style={{borderWidth:1,backgroundColor:'black'}} onPress={() => this.toggleshowPicker()}>
                        {this.state.showPicker && <DateTimePicker
                            mode="date"
                            display='spinner'
                            value={this.state.date}
                            onChange={this.changeDateTimePicker}
                        />}
                        <Text style={{fontSize:18,fontWeight:'700',color:'white'}}>Select Date Of Birth</Text>
                    </Pressable>
                    <Text style={{ fontSize: 20, fontWeight: '600' }}>{this.state.getDate}</Text>
                    <Pressable onPress={()=>this.getdoc()} style={{justifyContent:'center',alignItems:'center' ,width:200,height:40,backgroundColor:'lightblue'}}>
                    <Text style={{fontSize:18,fontWeight:'600'}}>Upload Document</Text>
                    </Pressable>
                    <Text>{this.state.docName}</Text>
                </View>
            </View>
        )
    }
    // source={this.state.getImage?require(this.state.getImage):require('../assets/addImage.png')}
    // source={this.state.getImage}
    changeDateTimePicker = ({ type }, selectDate) => {
        if (type == 'set') {
            this.setState({ getDate: selectDate.toDateString() })
        }
    }
    getdoc= async()=>{
        let result = await DocumentPicker.getDocumentAsync({})
        console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&",result.assets[0].name)
        this.setState({docName:result.assets[0].name})
    }
    openModal = () => {
        return (

            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isModalVisible}
                    onRequestClose={() => this.setState({ isModalVisible: false })}
                    presentationStyle="formSheet"
                >
                    <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center' }}>
                        <View>
                            <View style={{ flexDirection: 'row' }}>
                                <Button title="Open Galary" onPress={this.openGalary} color='lightblue' />
                                <Button title="Open Cemera" onPress={this.openCemera} color='lightblue' />
                            </View>
                            <Button title="Cancel" onPress={() => { this.setState({ isModalVisible: !this.state.isModalVisible }) }} />
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
    openGalary = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        console.log("000000000000000000000000000000000",result.assets[0].uri)
        this.setState({ getImage: result.assets[0].uri, isModalVisible: false })
    }

    openCemera=async()=>{
     await ImagePicker.requestCameraPermissionsAsync();
     let result  = await ImagePicker.launchCameraAsync({
        cameraType:ImagePicker.CameraType.front,
        allowsEditing:true,
        aspect:[1,1],
        quality:1
     })
     console.log("******************************************",result);
     this.setState({ getImage: result.assets[0].uri, isModalVisible: false })
    }
    toggleshowPicker = () => {
        if (this.state.showPicker == true) {
            this.setState({ showPicker: false })
        } else {
            this.setState({ showPicker: true })
        }

    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderMainBody()}
                {this.openModal()}
            </View>
        )
    }
}