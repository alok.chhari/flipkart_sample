import React, { Component } from 'react';
import { View, Text, ActivityIndicator, Alert, StyleSheet, Pressable, TextInput, FlatList,RefreshControl, PixelRatio} from 'react-native';
import Scale from './Scale';
import { LinearGradient } from 'expo-linear-gradient';
export default class Miscellaneous extends Component {
    constructor() {
        super()
        this.state = {
            golgol: false,
            showAlert: false,
            getTextInput: '',
            refreshing:false,
        }
    }
    renderGolgol = () => {
        return (
            <View>
                <Pressable style={styles.buttonStyle} onPress={this.handlegolgol}>
                    <Text style={styles.buttonText}>{this.state.golgol ? 'Stop' : 'GolGol Start'}</Text>
                </Pressable>
                <View style={styles.golgolMain}>
                {this.state.golgol ? 
                    <View style={styles.golgolStyles}>
                    <ActivityIndicator color='black' animating={true} size="large" /> 
                    </View>
                    :
                     ""}
                     </View>
            </View>
        )
    }
    handlegolgol = () => {
        if (this.state.golgol == false) {
            this.setState({ golgol: true })
        } else {
            this.setState({ golgol: false })
        }
    }
    renderAlert = () => {
        return (
            <View>
                <Pressable style={styles.buttonStyle} onPress={this.showAlert}>
                    <Text style={styles.buttonText}>{this.state.showAlert ? 'Stop' : 'Show Alert'}</Text>
                </Pressable>
            </View>
        )
    }
    showAlert = () => {
        Alert.alert('Heading', 'SubHeading', [
            {
                text: 'Cancel',
                onPress: () => this.setState({ showAlert: false }),
                style: 'cancel',
            },
            {
                text: 'OK',
                onPress: () => this.setState({ showAlert: false }),
                style: 'OK',
            }
        ]);
    }

    textInputElement = () => {
        return (
            <LinearGradient style={styles.textInputView} colors={["#cdeba9", "#a9d1eb", "#e1a9eb"]}>
                <View style={styles.textInputAndShowtext}>
                    <Text style={styles.showText}>{this.state.getTextInput}</Text>
                    <TextInput placeholder='Search...' value={String} style={styles.textInputStyle} onChangeText={(iptext) => this.setState({ getTextInput: iptext })} />
                </View>
            </LinearGradient>
        )
    }
    renderRefreshControl=()=>{
        return(
            <View style={styles.flatlistWithRefreshControl}>
            <FlatList
              data={[
                {key: 'Devin'},
                {key: 'Dan'},
                {key: 'Dominic'},
                {key: 'Jackson'},
                {key: 'James'},
                {key: 'Joel'},
                {key: 'John'},
                {key: 'Jillian'},
                {key: 'Jimmy'},
                {key: 'Julie'},
              ]}
              renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
              refreshControl={
                <RefreshControl refreshing={this.state.refreshing} progressBackgroundColor='pink' size='large' onRefresh={()=>this.onRefresh()} />
              }
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            />
          </View>
        )
    }
    onRefresh=()=>{
        this.setState({refreshing:true})
        setTimeout(()=>{
            this.setState({refreshing:false})
        },2000)
    }
    render() {
        return (
            <View style={styles.container}>
                {this.renderGolgol()}
                {this.renderRefreshControl()}
               
                {this.renderAlert()}
                {this.textInputElement()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'space-around',
        alignItems: 'center',
    },
    buttonStyle: {
        width: PixelRatio.getPixelSizeForLayoutSize(150),
        height: 50,
        backgroundColor: '#acbfb1',
        borderRadius: 10,
        shadowOffset: { width: 0, height: 4 },
        shadowColor: 'black',
        shadowOpacity: 0.6,
        shadowRadius: 3,
        margin: 10,
        elevation: 3,
        justifyContent: 'center',
        alignItems: 'center'
    },
    golgolStyles:{
      position:'absolute',
      top:0,
      bottom:0,
      left:0,
      right:0,
    //   justifyContent:'center',
    //   alignContent:'center',
      alignSelf:'center',
    //   zIndex:1,
    },
    golgolMain:{
        position:'absolute',
        top:0,
        bottom:0,
        left:0,
        right:0,
        flex:1,
        justifyContent:'center',
        alignContent:'center',
        alignSelf:'center',
    },
    buttonText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '700'
    },
    textInputStyle: {
        width: Scale(300),
        height: Scale(70),
        borderWidth: 0.1,
        borderRadius: 10,
        paddingHorizontal: 30,
    },
    showText: {
        fontSize: 20,
        fontWeight: "700",
    },
    textInputView: {
        flex: 0.3,
        marginHorizontal: Scale(40),
        borderRadius: 30,
    },
    textInputAndShowtext: {
        justifyContent: 'space-around',
        alignItems: 'center',
        flex: 1,
    },
    item:{
        padding: 10,
        fontSize: 18,
        height: 44,
      },
      flatlistWithRefreshControl:{
        flex:0.1,
      },
      screenDimensionsDisplay:{
        fontSize:18,
        fontWeight:"700",
      }
})
