import React, { Component } from 'react';
import { flipkartLogo, mike, cemera, flipsearchIcon, add01, add02, add03, add04, add001, add002, add003, add004, clipArt01, clipArt02, clipArt03, clipArt04, clipArt05, clipArt06, clipArt07, clipArt08, clipArt09, clipArt10, bgImage } from './assets'
import { Text, StyleSheet, View, TouchableOpacity, Image, TextInput, ScrollView, FlatList, ImageBackground } from 'react-native'
import Scale from './Scale';

export default class ListWithBackground extends Component{
    constructor(){
        super()
        this.state={
            sponsoredArray: [
                {
                  recentAddImage: require('../assets/Spotshirt.png'),
                  text: "Addidas T-Shirt",
                  price: "$1600",
                },
                {
                  recentAddImage: require('../assets/spoSmartWatch.png'),
                  text: "Fit Smart Watch",
                  price: "$1700",
                },
                {
                  recentAddImage: require('../assets/spoSocks.png'),
                  text: "Rebook Socks",
                  price: "$1800",
                },
                {
                  recentAddImage: require('../assets/spoVest.png'),
                  text: "Jockey Vest",
                  price: "$1900",
                },
                {
                  recentAddImage: require('../assets/spoToy.png'),
                  text: "Toys",
                  price: "$1000",
                },
                {
                  recentAddImage: require('../assets/spoCap.png'),
                  text: "Under Armar Cap",
                  price: "$2000",
                }],
        }
    }
    backgroundList = (item) => {
        return (
          <View style={styles.backGroundListMainView}>
            <View>
              <Image source={item.recentAddImage} resizeMode='cover' style={styles.listImage} />
              <Text style={styles.text01}>{item.text}</Text>
              <Text style={[styles.special, { color: 'green' }]}>Special</Text>
            </View>
          </View>
        )
      }
    render(){
        return(
            <ImageBackground source={bgImage} resizeMode='cover' style={styles.listBackground}>
            <View style={styles.suggestHeading}>
              <Text style={styles.recentText}>Best Quality</Text>
              <TouchableOpacity style={styles.rightArrowBlueColor}>
                <Text style={styles.rightArrow}>&#62;</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              numColumns={2}
              data={this.state.sponsoredArray}
              renderItem={({ item }) => this.backgroundList(item)}
            />
          </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    filpkartImage: {
      height: Scale(25),
      width: Scale(25),
      resizeMode: "contain",
    },
    flipGrocImageView: {
      height: Scale(50),
      width: Scale(180),
      justifyContent: "center",
      alignItems: "center",
      flexDirection: 'row',
      borderRadius: Scale(10),
    },
    flipkartLogoText: {
      color: 'white',
      fontSize: Scale(12),
      fontWeight: '600',
      fontStyle: 'italic',
    },
    flipkartGroceryView: {
      height: Scale(50),
      width: Scale(180),
      justifyContent: "center",
      alignItems: "center",
      flexDirection: 'row',
      borderRadius: Scale(10),
    },
    groceryLogoText: {
      color: 'black',
      fontSize: Scale(12),
      fontWeight: '600',
      fontStyle: 'italic',
  
    },
    headerView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: Scale(400),
      marginTop: Scale(10),
    },
    headerMainView: {
      justifyContent: 'center',
      flexDirection: 'row',
      marginHorizontal: Scale(20),
    },
    toggleON: {
      backgroundColor: '#E6E6E9', width: Scale(50), height: Scale(18), borderRadius: Scale(30), flexDirection: "row-reverse", alignItems: 'center', paddingVertical: Scale(1), paddingHorizontal: Scale(2)
    },
    toggleOnBlank: {
      width: Scale(14), height: Scale(14), borderRadius: Scale(30), backgroundColor: "#fff"
    },
    toggleONText: {
      fontSize: Scale(10),
    },
    toggleOff: {
      backgroundColor: '#E6E6E9', width: Scale(50), height: Scale(18), borderRadius: Scale(30), flexDirection: "row", alignItems: 'center', paddingVertical: Scale(1), paddingHorizontal: Scale(2),
    },
    toggleOffBlank: {
      width: Scale(14), height: Scale(14), borderRadius: Scale(30), backgroundColor: "#fff"
    },
    toggleOffText: {
      fontSize: Scale(10),
    },
    brandMall: {
      fontSize: Scale(12),
      color: "#4a4a48",
      marginBottom: Scale(8),
    },
    searchAndSwitch: {
      marginTop: Scale(12),
      marginHorizontal: Scale(15),
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    searchWithMikeAndCemera: {
      borderWidth: Scale(1),
      width: Scale(300),
      borderColor: '#9c9c98',
      borderRadius: Scale(10),
      paddingVertical: Scale(5),
      paddingHorizontal: Scale(5),
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: '#fff',
    },
    mikeStyle: { width: Scale(20), height: Scale(20) },
    cemeraStyle: { width: Scale(20), height: Scale(20), },
    TextInputStyle: {
      width: Scale(200),
      backgroundColor: '#fff',
    },
    searchIcon: {
      width: Scale(20), height: Scale(20)
    },
    pagerViewSubView: {
      justifyContent: 'center', alignItems: 'center'
    },
    pagerViewImage: {
      width: '100%', height: Scale(150),
    },
    pagerViewMainView:
      { height: Scale(150), marginTop: Scale(15) },
    clipArtImage: {
      width: Scale(50),
      height: Scale(50),
    },
    clipArtImageView: {
      width: Scale(70),
      height: Scale(70),
      backgroundColor: '#d2dafa',
      borderRadius: Scale(50),
      justifyContent: "center",
      alignItems: 'center',
    },
    clipArtMainView: {
      width: Scale(70),
      height: Scale(90),
      alignItems: 'center',
      margin: Scale(10)
    },
    scrollviewMain: {
      marginTop: Scale(10),
      height: Scale(130),
    },
    scrollViewText: {
      fontSize: Scale(10),
      fontWeight: '600',
      marginTop: Scale(5)
    },
    text01: { fontSize: Scale(11) },
    imageAddvertisement: { width: Scale(100), height: Scale(90) },
    imageAdvertisementView: { backgroundColor: '#d2dafa', height: Scale(100), width: Scale(110), justifyContent: 'center', alignItems: 'center' },
    advertisementSubView: { width: Scale(120), height: Scale(140), alignItems: 'center' },
    advertisementMainView: { padding: Scale(10) },
    recentMainView: { width: Scale(100), height: Scale(120), justifyContent: 'space-between', alignItems: 'center', borderWidth: Scale(1), borderColor: "#e1e3e1", marginRight: Scale(10), marginTop: Scale(5), },
    recentImage: { width: Scale(90), height: Scale(90), backgroundColor: '#fff' },
    recentText: {
      fontSize: Scale(18),
      fontWeight: "500",
      marginBottom: Scale(10),
    },
    recentFlatListView: {
      marginLeft: Scale(10),
      marginTop: Scale(10),
    },
    renderSponsoredMainView: {
      marginRight: Scale(20),
      marginTop: Scale(10)
    },
    rightArrowBlueColor: {
      width: Scale(30),
      height: Scale(30),
      backgroundColor: '#5b60f0',
      borderRadius: Scale(30),
      justifyContent: 'center',
      alignItems: 'center',
    },
    suggestHeading: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: Scale(10),
      marginHorizontal: Scale(10),
    },
    rightArrow: {
      color: 'white',
      fontWeight: '600',
    },
    suggestionImage: {
      width: Scale(190),
      height: Scale(100),
    },
    suggestionMainView: { borderWidth: Scale(0.7), borderColor: "#7e807f", width: Scale(130), height: Scale(160), alignItems: 'center', marginRight: Scale(10) },
    suggestionSubView: { flexDirection: 'row', justifyContent: 'space-between' },
    listImage: {
      width: Scale(150),
      height: Scale(180),
      marginVertical: Scale(7),
    },
    special: {
      fontSize: 13,
      fontWeight: '600'
    },
    listBackground: {
      marginTop: Scale(10),
      marginBottom: Scale(20),
    },
    backGroundListMainView: { borderColor: "#7e807f", width: Scale(190), height: Scale(250), borderWidth: 0.7, backgroundColor: "#fff", marginHorizontal: Scale(10), alignItems: 'center', marginBottom: Scale(10), marginTop: Scale(10), borderRadius: 10, paddingVertical: Scale(5) }
  
  })