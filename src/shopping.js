import React, { Component } from 'react';
import { SafeAreaView, View, Text, Image, TextInput, ScrollView, StyleSheet, FlatList, Modal, TouchableWithoutFeedback } from 'react-native';
import { profilePic, navMark, bell, searchIcon, filter,home,cart,person,heart } from './assets';
import Scale from './Scale';
export default class Shopping extends Component {
    constructor() {
        super()
        this.state = {
            categoriesData: [
                {
                    image: require("../assets/cup.png"),
                    text: 'Cappuccino'
                },
                {
                    image: require("../assets/cup.png"),
                    text: 'Coffee'
                },
                {
                    image: require("../assets/cup.png"),
                    text: 'Expresso'
                },
                {
                    image: require("../assets/cup.png"),
                    text: 'Cold Brew'
                },
                {
                    image: require("../assets/cup.png"),
                    text: 'Doppio'
                },
                {
                    image: require("../assets/cup.png"),
                    text: 'Affogato'
                },
                {
                    image: require("../assets/cup.png"),
                    text: 'Cortado'
                },
                {
                    image: require("../assets/cup.png"),
                    text: 'Flat White'
                },
            ],
            specialOfferCard: [
                {
                    image: require("../assets/coffee01.png"),
                    text: 'Cappuccino',
                    image02: require("../assets/plusIcon.png"),
                    price: "30.000",
                },
                {
                    image: require("../assets/coffee02.png"),
                    text: 'Coffee',
                    image02: require("../assets/plusIcon.png"),
                    price: "20.000",
                },
                {
                    image: require("../assets/coffee01.png"),
                    text: 'Expresso',
                    image02: require("../assets/plusIcon.png"),
                    price: "40.000",
                },
                {
                    image: require("../assets/coffee02.png"),
                    text: 'Cold Brew',
                    image02: require("../assets/plusIcon.png"),
                    price: "60.000",
                },
                {
                    image: require("../assets/coffee01.png"),
                    text: 'Doppio',
                    image02: require("../assets/plusIcon.png"),
                    price: "70.000",
                },
                {
                    image: require("../assets/coffee02.png"),
                    text: 'Affogato',
                    image02: require("../assets/plusIcon.png"),
                    price: "90.000",
                },
                {
                    image: require("../assets/coffee01.png"),
                    text: 'Cortado',
                    image02: require("../assets/plusIcon.png"),
                    price: "50.000",
                },
                {
                    image: require("../assets/coffee02.png"),
                    text: 'Flat White',
                    image02: require("../assets/plusIcon.png"),
                    price: "10.000",
                },
            ],
            nonspecialOfferCard: [
                {
                    image: require("../assets/coffee01.png"),
                    text: 'Cappuccino',
                    image02: require("../assets/plusIcon.png"),
                    price: "30.000",
                },
                {
                    image: require("../assets/coffee02.png"),
                    text: 'Coffee',
                    image02: require("../assets/plusIcon.png"),
                    price: "20.000",
                },
                {
                    image: require("../assets/coffee01.png"),
                    text: 'Expresso',
                    image02: require("../assets/plusIcon.png"),
                    price: "40.000",
                },
                {
                    image: require("../assets/coffee02.png"),
                    text: 'Cold Brew',
                    image02: require("../assets/plusIcon.png"),
                    price: "60.000",
                },
                {
                    image: require("../assets/coffee01.png"),
                    text: 'Doppio',
                    image02: require("../assets/plusIcon.png"),
                    price: "70.000",
                },
                {
                    image: require("../assets/coffee02.png"),
                    text: 'Affogato',
                    image02: require("../assets/plusIcon.png"),
                    price: "90.000",
                },
                {
                    image: require("../assets/coffee01.png"),
                    text: 'Cortado',
                    image02: require("../assets/plusIcon.png"),
                    price: "50.000",
                },
                {
                    image: require("../assets/coffee02.png"),
                    text: 'Flat White',
                    image02: require("../assets/plusIcon.png"),
                    price: "10.000",
                },
            ]
        }
    }
    renderHeader() {
        return (
            <SafeAreaView>
                <View style={{ marginHorizontal: Scale(29), marginTop: Scale(45), flex: 0.5, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Image source={profilePic} />
                    <View style={{ flexDirection: 'row', height: 20, marginTop: 7 }}>
                        <Image style={{ marginTop: 5, marginRight: 4 }} source={navMark} />
                        <Text style={{ fontSize: 12, fontWeight: '500' }}>New Delhi, India</Text>
                    </View>
                    <Image style={{ marginTop: 4 }} source={bell} />
                </View>
                <View style={{ marginTop: 50, marginHorizontal: 29 }}>
                    <Text style={{ fontSize: 14, fontWeight: '600' }}>Good Morning, Alok</Text>
                </View>
                <View style={{ marginHorizontal: 29, flexDirection: 'row', justifyContent: 'center', marginTop: 30 }}>
                    <View style={{ borderRadius: 60, backgroundColor: 'rgb(244,244,244)', height: 55, width: 350, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={searchIcon} />
                        <TextInput style={{ width: 250 }} />
                        <Image source={filter} />
                    </View>
                </View>
                <View style={{ marginTop: 21, marginHorizontal: 29 }}>
                    <Text style={{ fontSize: 14, fontWeight: '600' }}>Catagories</Text>
                </View>
            </SafeAreaView>

        )
    }
    renderModal = () => {
        return (
            <TouchableWithoutFeedback>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.isModalVisible}
                    onRequestClose={() => this.setState({isModalVisible:false})}
                    presentationStyle="pageSheet"
                >
                    <View>
                    </View>

                </Modal>
            </TouchableWithoutFeedback>

        )
    }
    render = () => {
        return (
            <View style={{ flex: 1, backgroundColor: "#fff" }}>
                <ScrollView >
                    {this.renderHeader()}
                    {this.renderCategories()}
                    {this.renderSpecialOffer()}
                    {this.renderNonSpecialOffer()}
                </ScrollView>
                {this.bottomNevigation()}
            </View>

        )
    }



    renderCategories = () => {
        return (
            <View style={styles.coffeeFlatlist}>
                <FlatList
                    horizontal={true}
                    data={this.state.categoriesData}
                    renderItem={({ item }) => this.coffeeCategories(item)}
                />
            </View>
        )
    }


    coffeeCategories = (item) => {
        return (
            <View style={styles.coffies}>
                <Image source={item.image} />
                <Text style={styles.coffiesTextColor}>{item.text}</Text>
            </View>
        )
    }

    renderSpecialOffer = () => {
        return (
            <View style={styles.coffeeFlatlistRender}>
                <FlatList showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    data={this.state.specialOfferCard}
                    renderItem={({ item }) => this.specialOffer(item)}
                />
            </View>
        )
    }

    specialOffer = (item) => {
        return (
            <View style={styles.specialOfferCardStyle}>
                <View style={styles.coffeeImageView}>
                    <Image source={item.image} style={styles.coffeeImage} />
                </View>
                <View style={styles.specialOfferContent}>
                    <View>
                        <Text style={styles.specialOfferText}>{item.text}</Text>
                        <Text style={styles.withSugar}>With Sugar</Text>
                        <View style={styles.priceWithImage}>
                            <View style={styles.tagWithPrice}>
                                <Text style={styles.Rp}>Rp</Text>
                                <Text style={styles.specialOfferPrice}>60.000</Text>
                            </View>
                            <Image source={item.image02} style={styles.plusImage} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    renderNonSpecialOffer = () => {
        return (
            <View style={styles.nonSpecialOffer}>
                <Text style={styles.nonSpecialOfferText}>Special Offer</Text>
                <FlatList
                    numColumns={2}
                    data={this.state.nonspecialOfferCard}
                    renderItem={({ item }) => this.nonSpecialOfferCards(item)}
                    ItemSeparatorComponent={<View style={{height:10,backgroundColor:'black'}}/>}
                />
            </View>

        )
    }
    nonSpecialOfferCards = (item) => {
        return (
            <View style={styles.specialOfferCardStyles}>
                <View style={styles.coffeeImageView}>
                    <Image source={item.image} style={styles.coffeeImage} />
                </View>
                <View style={styles.specialOfferContent}>
                    <View>
                        <Text style={styles.specialOfferText}>{item.text}</Text>
                        <Text style={styles.withSugar}>With Sugar</Text>
                        <View style={styles.priceWithImage}>
                            <View style={styles.tagWithPrice}>
                                <Text style={styles.Rp}>Rp</Text>
                                <Text style={styles.specialOfferPrice}>60.000</Text>
                            </View>
                            <Image source={item.image02} style={styles.plusImage} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
    bottomNevigation = () => {
        return (
            <View style={styles.bottomNevView}>
                <View style={styles.bottomCategoriesView}>
                <Image source={home} style={styles.shape}/>
                <Image source={cart} style={styles.shape}/>
                <Image source={person} style={styles.shape}/>
                <Image source={heart} style={styles.shape}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    coffies: {
        width: 105,
        height: 33,
        borderRadius: 60,
        backgroundColor: 'rgb(0,88,47)',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: Scale(10),
        marginRight: Scale(7),
    },
    coffiesTextColor: {
        color: 'white',
        fontSize: 10,
        fontWeight: '600',
    },
    coffeeFlatlist: {
        marginLeft: Scale(29),
    },
    specialOfferCardStyle: {
        width: Scale(152),
        height: Scale(192),
        borderRadius: 8,
        marginRight: 11,
        mmarginTop: 11,
        marginBottom: 11,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: "#fff"

    },
    specialOfferCardStyles: {
        width: Scale(182),
        height: Scale(192),
        borderRadius: 8,
        // marginTop: 11,
        // marginBottom: 11,
        flex: 1,
        // justifyContent: "space-around",
        margin:11,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        elevation: 3,
        // backgroundColor: "#fff"
        backgroundColor:'red'

    },
    plusImage: {
        width: Scale(33),
        height: Scale(33),
    },
    priceWithImage: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: Scale(128),
    },
    coffeeImage: {
        width: Scale(144),
        height: Scale(105),
        borderRadius: 20,
        marginTop: 10
    },
    coffeeFlatlistRender: {
        marginTop: Scale(28),
        marginLeft: Scale(29),
    },
    coffeeImageView: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    specialOfferText: {
        fontSize: Scale(14),
        fontWeight: '600',
    },
    withSugar: {
        fontSize: Scale(10),
        fontWeight: '400',
    },
    specialOfferContent: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    Rp: {
        fontSize: Scale(12),
        fontWeight: "600",
    },
    specialOfferPrice: {
        fontSize: Scale(18),
        fontWeight: "600",
        marginTop: 5,
    },
    tagWithPrice: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    nonSpecialOffer: {
        marginTop: 21,
        marginHorizontal: 29,
    },
    nonSpecialOfferText: {
        fontSize: 14,
        fontWeight: '600'
    },
    bottomNevView: {
        flex: 1,
        justifyContent: 'flex-end',
        marginTop:100,
        // width:'100%',
    },
    bottomCategoriesView: {
        height: Scale(99),
        width: "100%",
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        flexDirection:'row',
        justifyContent:"space-around",
        alignItems:'center',
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: -1,
        // },
        // shadowOpacity: 0.6,
        // shadowRadius: 2,
        // // elevation:4,
        // elevation: 4,
        // backgroundColor: "#fff",
        height:Scale(99),width:"100%",alignItems:"center",borderRadius:Scale(12),shadowColor: "black",
       shadowOpacity: 0.3,
       shadowRadius:2,
       shadowOffset: {
         height: -1,
         width: 0,
       },
       elevation: 3,
    },
    shape:{
        width:Scale(23),
        height:Scale(26),
    }
})