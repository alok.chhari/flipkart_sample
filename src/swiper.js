import React, { Component } from 'react';
import PagerView from 'react-native-pager-view';
import { LinearGradient } from 'expo-linear-gradient';
import { View, Text,StyleSheet ,Button ,alert} from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
export default class SwiperMode extends Component {
    constructor() {
        super()
        this.state = {
            hasPermission: null,
            scannned: false,
        }
    }
    componentDidMount() {
        this.getBarCodeScannerPermissions();
      }
    getBarCodeScannerPermissions = async () => {
        // this.setState({ hasPermission: await BarCodeScanner.requestPermissionsAsync()})
       let getPermission =  await BarCodeScanner.requestPermissionsAsync()
       console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^",getPermission);
    };
    handleBarCodeScanned = ({ type, data }) => {
        // alert(`Bar code with type ${type} and data ${data} has been scanned!`);
        console.log(`@@@@@@@@@@@@@@@Data=${data} , type=${type}`);
    };
    render() {
        const { hasPermission, scanned } = this.state;
        return (
            <View style={styles.container}>
                <PagerView style={styles.pageViewerStyle} initialPage={0}>
                    <LinearGradient style={styles.gradientColor} colors={['#f0dca5', '#bff0a5', '#192f6a']}>
                        <View style={styles.subContainer} key='1'>
                            <Text style={styles.FontStyles}>I -First</Text>
                        </View>
                    </LinearGradient>
                    <LinearGradient style={styles.gradientColor} colors={['#a5f0d4', '#a5cff0', '#c7a5f0']}>
                        <View style={styles.subContainer} key='2'>
                            <Text style={styles.FontStyles}>II -Second</Text>
                        </View>
                    </LinearGradient>
                    <LinearGradient style={styles.gradientColor} colors={['#f0a5ec', '#f0a5bb', '#f0a5a5']}>
                        <View style={styles.subContainer} key='3'>
                            <BarCodeScanner
                                onBarCodeScanned={this.handleBarCodeScanned}
                                style={StyleSheet.absoluteFillObject}
                            />
                        </View>
                    </LinearGradient>
                </PagerView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    pageViewerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    subContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    gradientColor: {
        flex: 1,
    },
    FontStyles: {
        fontSize: 25,
        fontWeight: "900",
    },
})