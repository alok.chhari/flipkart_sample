import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer'; 4
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View, Text } from 'react-native';
import Home from './home'
import Shopping from './shopping';
import CoffeeDetails from './coffeeDetail';
import Cart from './cart';
import MyComponent from './nativeComponents';
import SwiperMode from './swiper';
import Miscellaneous from './miscellaneous'
import SignUp from './SignUp';
import Flipkart from './Flipkart';
import { Entypo } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import AddidasTShirt from './addidas'
const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();
const screenOptions = {
  tabBarShowLabel: false,
  headerShown: false,
  tabBarStyle: {
    position: "relative",
    bottom: 0,
    right: 0,
    left: 0,
    elevation: 0,
    height: 60,
    background: "#fff",
    
  }
}
export default class PageNavigation extends Component {
  render() {
    return (
      <>
        {/* <NavigationContainer>
                <Stack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
                    <Stack.Screen name="CoffeeDetails" component={CoffeeDetails} />
                    <Stack.Screen name="Shopping" component={Shopping} />
                    <Stack.Screen name="Home" component={Home} />
                    <Stack.Screen name="cart" component={Cart} />
                </Stack.Navigator>
            </NavigationContainer> */}

        {/* <NavigationContainer>
               <Drawer.Navigator initialRouteName='Flipkart'>
                <Drawer.Screen name='Home' component={Home}/>
                <Drawer.Screen name='CoffeeDetails' component={CoffeeDetails}/>
                <Drawer.Screen name='Shopping' component={Shopping}/>
                <Drawer.Screen name='Cart' component={Cart}/>
                <Drawer.Screen name='MyComponent' component={MyComponent}/>
                <Drawer.Screen name='Swiper' component={SwiperMode}/>
                <Drawer.Screen name='Miscellaneous' component={Miscellaneous}/>
                <Drawer.Screen name='SignUp' component={SignUp}/>
                <Drawer.Screen name='Flipkart' component={Flipkart}/>
                </Drawer.Navigator> 
            </NavigationContainer> */}

        <NavigationContainer>
          <Tab.Navigator screenOptions={screenOptions}>
          <Tab.Screen
              name="AddidasTShirt"
              component={AddidasTShirt}
              options={{
                tabBarIcon: ({ focused }) => {
                  return (
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                      <Ionicons name="settings" size={24} color={focused ? "#16247d" : "#111"} />
                      <Text style={{ fontSize: 12, color: "#16247d" }}>AddidasTShirt</Text>
                    </View>
                  )
                }
              }}
            />
            <Tab.Screen
              name="Home"
              component={Home}
              options={{
                tabBarIcon: ({ focused }) => {
                  return (
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                      <Entypo name="home" size={24} color={focused ? "#16247d" : "#111"} />
                      <Text style={{ fontSize: 12, color: "#16247d" }}>HOME</Text>
                    </View>
                  )
                }
              }}
            />
            <Tab.Screen
              name="Coffee"
              component={CoffeeDetails}
              options={{
                tabBarIcon: ({ focused }) => {
                  return (
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                      <FontAwesome name="coffee" size={24} color={focused ? "#16247d" : "#111"} />
                      <Text style={{ fontSize: 12, color: "#16247d" }}>Coffee</Text>
                    </View>
                  )
                }
              }}
            />
            <Tab.Screen
              name="Shopping"
              component={Shopping}
              options={{
                tabBarIcon: ({ focused }) => {
                  return (
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                      <Entypo name="wallet" size={24} color={focused ? "#16247d" : "#111"} />
                      <Text style={{ fontSize: 12, color: "#16247d" }}>Shopping</Text>
                    </View>
                  )
                }
              }}
            />
            <Tab.Screen
              name="FlipKart"
              component={Flipkart}
              options={{
                tabBarIcon: ({ focused }) => {
                  return (
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                      <Entypo name="shopping-cart" size={24} color={focused ? "#16247d" : "#111"} />
                      <Text style={{ fontSize: 12, color: "#16247d" }}>FlipKart</Text>
                    </View>
                  )
                }
              }}
            />
            <Tab.Screen
              name="Miscellaneous"
              component={Miscellaneous}
              options={{
                tabBarIcon: ({ focused }) => {
                  return (
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                      <Ionicons name="settings" size={24} color={focused ? "#16247d" : "#111"} />
                      <Text style={{ fontSize: 12, color: "#16247d" }}>Miscellan</Text>
                    </View>
                  )
                }
              }}
            />
           
          </Tab.Navigator>
        </NavigationContainer>
      </>
    )
  }
}