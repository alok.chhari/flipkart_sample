import React, { Component } from 'react'
import { SafeAreaView, Text, Image, View, TextInput, ScrollView, StyleSheet, FlatList, Modal, TouchableWithoutFeedback, ImageBackground, TouchableOpacity } from 'react-native';
import { coffeeWithPlat, leftArrow, emptyHeart, star, line } from './assets';
import Scale from './Scale'
import ReadMore from '@fawazahmed/react-native-read-more';
export default class CoffeeDetail extends Component {
    constructor(){
        super()
        this.state={
            small:true,
            medium:false,
            large:false,
            noSugar:true,
            mediumSugar:false,
            fullSugar:false,
        }
    }
    renderHeader = () => {
        return (
            <View style={styles.addtocartdetails}>
            <ScrollView>
                    <Text style={styles.cupSize}>Cup Size</Text>
                <View style={styles.SML}>
                    <TouchableOpacity style={[styles.smallCup,{backgroundColor: this.state.small? '#00512C':"#fff"}]} onPress={()=>this.setState({small:true,medium:false,large:false})}>
                        <Text style={[styles.medium,{color:this.state.small?"#fff":"black"}]}>Small</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.smallCup,{backgroundColor: this.state.medium? '#00512C':"#fff"}]} onPress={()=>this.setState({small:false,medium:true,large:false})}>
                        <Text style={[styles.medium,{color:this.state.medium?"#fff":"black"}]}>Medium</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.smallCup,{backgroundColor: this.state.large? '#00512C':"#fff"}]} onPress={()=>this.setState({small:false,medium:false,large:true})}>
                        <Text style={[styles.medium,{color:this.state.large?"#fff":"black"}]}>Large</Text>
                    </TouchableOpacity>
                </View>

                <View>
                    <Text style={styles.cupSize}>Sugar Level</Text>
                </View>
                <View style={styles.SML}>
                    <TouchableOpacity style={[styles.smallCup,{backgroundColor: this.state.noSugar? '#00512C':"#fff"}]}  onPress={()=>this.setState({noSugar:true,mediumSugar:false,fullSugar:false})} >
                        <Text  style={[styles.medium,{color:this.state.noSugar?"#fff":"black"}]}>No Sugar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.smallCup,{backgroundColor: this.state.mediumSugar? '#00512C':"#fff"}]} onPress={()=>this.setState({noSugar:false,mediumSugar:true,fullSugar:false})}>
                        <Text  style={[styles.medium,{color:this.state.mediumSugar?"#fff":"black"}]}>Low</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.smallCup,{backgroundColor: this.state.fullSugar? '#00512C':"#fff"}]} onPress={()=>this.setState({noSugar:false,mediumSugar:false,fullSugar:true})}>
                        <Text  style={[styles.medium,{color:this.state.fullSugar?"#fff":"black"}]} >Medium</Text>
                    </TouchableOpacity>
                </View>

                <View>
                    <Text style={styles.cupSize}>About</Text>
                </View>
                <View style={styles.RMRLText}>
                <ReadMore numberOfLines={3}>
                {
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut porttitor nisi, quis venenatis orci. Nunc posuere interdum dui, ac egestas augue condimentum dignissim. Sed eu ligula a leo efficitur fermentum non non mauris. Duis pellentesque tortor nisl, in iaculis eros ullamcorper a." 
                }
                </ReadMore>
                </View>
                    <TouchableOpacity style={styles.priceAndAddtoCart}>
                        <Text style={styles.addtoCart}>Add to Cart</Text>
                        <Image source={line} style={styles.vLine} />
                        <Text style={styles.addtoCart}>50.000</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        )
    }
    render() {
        return (

            <ImageBackground source={coffeeWithPlat} resizeMode='cover' style={styles.maincontainer}>
                <View style={styles.leftArrowAndEmptyHeart}>
                    <TouchableOpacity style={styles.whiteBG}>
                        <Image source={leftArrow} style={styles.headerImage} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.whiteBG}>
                        <Image source={emptyHeart} style={styles.headerImage} />
                    </TouchableOpacity>
                </View>

                <View style={styles.coffeeWithSugarMainView}>
                    <View style={styles.coffeeWithRating}>
                        <View style={styles.coffeeWithSugar}>
                            <Text style={styles.coffeeName}>Cappuccino</Text>
                            <Text style={styles.withSugar}>With Sugar</Text>
                        </View>
                        <View style={styles.starRating}>
                            <Image source={star} style={styles.starWH} />
                            <Text style={styles.rating}>4.5</Text>
                        </View>
                    </View>
                </View>
                {this.renderHeader()}
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    maincontainer: {
        flex: 1,
        height: "100%",
        width: "100%",
    },
    container: {
        height: Scale(444),
        width: "100%",
        backgroundColor: "#fff",
        borderTopRightRadius: Scale(30),
        borderTopLeftRadius: Scale(30)
    },
    bgImage: {
        height: 619,
    },
    leftArrowAndEmptyHeart: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 33,
        marginTop: 33,
    },
    whiteBG: {
        backgroundColor: 'white',
        width: 33,
        height: 33,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerImage: {
        width: 26,
        height: 26,
    },
    coffeeName: {
        color: 'white',
        fontSize: 36,
        fontWeight: '600',
    },
    withSugar: {
        fontSize: 14,
        fontWeight: '500',
        color: 'white',
    },
    coffeeWithSugarMainView: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    starWH: {
        width: 20,
        height: 20,
        marginRight: 9,
    },
    starRating: {
        width: 77,
        height: 32,
        backgroundColor: '#C1925B',
        borderRadius: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
    },
    rating: {
        color: 'white',
        fontSize: 14,
        fontWeight: '500',
    },
    coffeeWithRating: {
        marginHorizontal: 33,
        marginBottom: 150,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    addtocartdetails: {
        borderTopLeftRadius: 30, borderTopRightRadius: 30, backgroundColor: 'white',width:"100%",flex:1
    },
    cupSize: {
        fontSize: 18,
        fontWeight: '600',
        marginTop: 24,
        marginHorizontal: 34,
    },
    small: {
        fontSize: 18,
        fontWeight: '500',
    },
    medium: {
        fontSize: 18,
        fontWeight: '500',
    },
    smallCup: {
        width: Scale(103),
        height: Scale(32),
        justifyContent: 'center',
        flexDirection: "row",
        borderRadius: 30,
    },
    SML: {
        marginHorizontal: 34,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 14,
    },
    RMRLText:{
        marginHorizontal: 34,
        marginTop: 14,
    },
    loremText: {
        fontSize: 12,
        fontWeight: '600',
    },
    priceAndAddtoCart: {
        backgroundColor: '#00512C',
        marginHorizontal: 34,
        borderRadius: 30,
        flexDirection: 'row',
        justifyContent: "space-evenly",
        alignItems: "center",
        height: 76,
        marginTop: 24,
        marginBottom:10,
    },
    addtoCart: {
        fontSize: 20,
        fontWeight:'600',
        color: 'white',
    },
    vLine: {
        height: 30,
        borderRadius: 20,
        width: 5,
    }
})