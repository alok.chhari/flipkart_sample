import { Text, StyleSheet, View, TouchableOpacity, Image, TextInput, ScrollView, FlatList, ImageBackground } from 'react-native'
import React, { Component } from 'react'
import Scale from './Scale'
import { flipkartLogo, mike, cemera, flipsearchIcon, add01, add02, add03, add04, add001, add002, add003, add004, clipArt01, clipArt02, clipArt03, clipArt04, clipArt05, clipArt06, clipArt07, clipArt08, clipArt09, clipArt10, bgImage } from './assets'
import PagerView from 'react-native-pager-view';
import AddidasTShirt from './addidas'
import Home from './home'
import Shopping from './shopping';
import CoffeeDetails from './coffeeDetail';
import Cart from './cart';
import MyComponent from './nativeComponents';
import SwiperMode from './swiper';
import Miscellaneous from './miscellaneous'
import SignUp from './SignUp';
import { LinearGradient } from 'expo-linear-gradient';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer'; 4
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import ListWithBackground from './listWithBackground'
const Tab = createBottomTabNavigator();
const screenOptions = {
  tabBarShowLabel: false,
  headerShown: false,
  tabBarStyle: {
    position: "relative",
    bottom: 0,
    right: 0,
    left: 0,
    elevation: 0,
    height: 60,
    background: "#fff",
    
  }
}
export default class Flipkart extends Component {

  constructor() {
    super()
    this.state = {
      bottomNavColor: {
        home: false,
        catagory: false,
        notification: false,
        account: false,
        cart: false,
      },
      headerButtonColor: false,
      toggleSwitch: false,
      advertisement: [{
        addimage: require("../assets/fashion01.png"),
        text01: "Lowest Price On",
        text02: "Lakhs of Products",
      },
      {
        addimage: require("../assets/airlines01.png"),
        text01: "SpiceJet Flights",
        text02: "From $1,200",
      },
      {
        addimage: require("../assets/earbuds01.png"),
        text01: "Best of Audio",
        text02: "Just $899",
      }],
      recentViewArray: [{
        recentAddImage: require('../assets/trimmer.png'),
        text: "trimmer",
        price: '$1200',
        delivery: 'Free Delivery',
        strickPrice: "$3200",
      },
      {
        recentAddImage: require('../assets/moto.png'),
        text: "Motorola",
        price: '$1300',
        delivery: 'Free Delivery',
        strickPrice: "$2200",
      },
      {
        recentAddImage: require('../assets/bata.png'),
        text: "Bata Shoes",
        price: '$1400',
        delivery: 'Free Delivery',
        strickPrice: "$1900",
      },
      {
        recentAddImage: require('../assets/earbuds.png'),
        text: "Sony Earbuds",
        price: '$1500',
        delivery: 'Free Delivery',
        strickPrice: "$1800",
      },
      {
        recentAddImage: require('../assets/tshirt.png'),
        text: "Addidas T-Shirt",
        price: '$1600',
        delivery: 'Free Delivery',
        strickPrice: "$1700",
      },
      {
        recentAddImage: require('../assets/smartWatch.png'),
        text: "Amaze Fit Smart",
        price: '$1700',
        delivery: 'Free Delivery',
        strickPrice: "$1600",
      },
      {
        recentAddImage: require('../assets/socks.png'),
        text: "Rebook Socks",
        price: '$1800',
        delivery: 'Free Delivery',
        strickPrice: "$1500",
      },
      {
        recentAddImage: require('../assets/vest.png'),
        text: "Jockey Vest",
        price: '$1900',
        delivery: 'Free Delivery',
        strickPrice: "$1400",
      },
      {
        recentAddImage: require('../assets/toy.png'),
        text: "Toys",
        price: '$2200',
        delivery: 'Free Delivery',
        strickPrice: "$1300",
      },
      {
        recentAddImage: require('../assets/cap.png'),
        text: "Under Armar Cap",
        price: '$3200',
        delivery: 'Free Delivery',
        strickPrice: "$1200",
      }],
      sponsoredArray: [
        {
          recentAddImage: require('../assets/Spotshirt.png'),
          text: "Addidas T-Shirt",
          price: "$1600",
        },
        {
          recentAddImage: require('../assets/spoSmartWatch.png'),
          text: "Fit Smart Watch",
          price: "$1700",
        },
        {
          recentAddImage: require('../assets/spoSocks.png'),
          text: "Rebook Socks",
          price: "$1800",
        },
        {
          recentAddImage: require('../assets/spoVest.png'),
          text: "Jockey Vest",
          price: "$1900",
        },
        {
          recentAddImage: require('../assets/spoToy.png'),
          text: "Toys",
          price: "$1000",
        },
        {
          recentAddImage: require('../assets/spoCap.png'),
          text: "Under Armar Cap",
          price: "$2000",
        }],
      // bottomNav: [{
      //   imageNav: require('../assets/homeIcon.png'),
      //   textNav: 'Home',
      // },
      // {
      //   imageNav: require('../assets/catIcon.png'),
      //   textNav: 'Catagory',
      // },
      // {
      //   imageNav: require('../assets/notIcon.png'),
      //   textNav: 'Notification',
      // },
      // {
      //   imageNav: require('../assets/accountIcon.png'),
      //   textNav: 'Account',
      // },
      // {
      //   imageNav: require('../assets/cartIcon.png'),
      //   textNav: 'Cart',
      // },
      // ],
    }
  }

  renderHeader = () => {
    return (
      <View>
        {this.state.toggleSwitch ?
          <LinearGradient colors={['#050505', '#545353']}>
            <View style={styles.headerMainView}>
              <View style={styles.headerView}>
                <TouchableOpacity style={[styles.flipGrocImageView, { backgroundColor: this.state.headerButtonColor ? "#E6E6E7" : "#047BD5" }]} onPress={() => this.handleHeaderColor(false)}>
                  <Image style={styles.filpkartImage} source={flipkartLogo} />
                  <Text style={[styles.flipkartLogoText, { color: this.state.headerButtonColor ? "black" : "white" }]}>Flipkart</Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.flipkartGroceryView, { backgroundColor: this.state.headerButtonColor ? "green" : "#E6E6E7" }]} onPress={() => this.handleHeaderColor(true)}>
                  <Image style={styles.filpkartImage} source={flipkartLogo} />
                  <Text style={[styles.groceryLogoText, { color: this.state.headerButtonColor ? "white" : "black" }]}>Grocery</Text>
                </TouchableOpacity>
              </View>
            </View>
          </LinearGradient>
          :
          <View style={styles.headerMainView}>
            <View style={styles.headerView}>
              <TouchableOpacity style={[styles.flipGrocImageView, { backgroundColor: this.state.headerButtonColor ? "#E6E6E7" : "#047BD5" }]} onPress={() => this.handleHeaderColor(false)}>
                <Image style={styles.filpkartImage} source={flipkartLogo} />
                <Text style={[styles.flipkartLogoText, { color: this.state.headerButtonColor ? "black" : "white" }]}>Flipkart</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[styles.flipkartGroceryView, { backgroundColor: this.state.headerButtonColor ? "green" : "#E6E6E7" }]} onPress={() => this.handleHeaderColor(true)}>
                <Image style={styles.filpkartImage} source={flipkartLogo} />
                <Text style={[styles.groceryLogoText, { color: this.state.headerButtonColor ? "white" : "black" }]}>Grocery</Text>
              </TouchableOpacity>
            </View>
          </View>
        }
      </View>
    )
  }
  handleHeaderColor = (getBoolean) => {
    this.setState({ headerButtonColor: getBoolean })
  }
  searchAndToggle = () => {
    return (
      <View>
        {this.state.toggleSwitch ?
          <LinearGradient colors={['#545353', '#fff']}>
            <View style={styles.searchAndSwitch}>
              <View style={styles.mallAndSwitch}>
                <Text style={styles.brandMall}>Brand Mall</Text>
                {this.state.toggleSwitch ?
                  <TouchableOpacity onPress={this.handleToggleSwitch} style={styles.toggleON}>
                    <View style={styles.toggleOnBlank}></View>
                    <Text style={styles.toggleONText}>ON</Text>
                  </TouchableOpacity>
                  :
                  <TouchableOpacity onPress={this.handleToggleSwitch} style={styles.toggleOff}>
                    <View style={styles.toggleOffBlank}></View>
                    <Text style={styles.toggleOffText}>OFF</Text>
                  </TouchableOpacity>
                }
              </View>
              <View style={styles.searchWithMikeAndCemera}>
                <TouchableOpacity><Image source={flipsearchIcon} style={styles.searchIcon} /></TouchableOpacity>
                <TextInput style={styles.TextInputStyle} />
                <TouchableOpacity><Image source={mike} style={styles.mikeStyle} /></TouchableOpacity>
                <TouchableOpacity><Image source={cemera} style={styles.cemeraStyle} /></TouchableOpacity>
              </View>
            </View>
          </LinearGradient>
          :
          <View style={styles.searchAndSwitch}>
            <View style={styles.mallAndSwitch}>
              <Text style={styles.brandMall}>Brand Mall</Text>
              {this.state.toggleSwitch ?
                <TouchableOpacity onPress={this.handleToggleSwitch} style={styles.toggleON}>
                  <View style={styles.toggleOnBlank}></View>
                  <Text style={styles.toggleONText}>ON</Text>
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={this.handleToggleSwitch} style={styles.toggleOff}>
                  <View style={styles.toggleOffBlank}></View>
                  <Text style={styles.toggleOffText}>OFF</Text>
                </TouchableOpacity>
              }
            </View>
            <View style={styles.searchWithMikeAndCemera}>
              <TouchableOpacity>
              <Image source={flipsearchIcon} style={styles.searchIcon} />
              </TouchableOpacity>
              <TextInput style={styles.TextInputStyle} />
              <TouchableOpacity>
              <Image source={mike} style={styles.mikeStyle} />
              </TouchableOpacity>
              <TouchableOpacity>
              <Image source={cemera} style={styles.cemeraStyle} />
              </TouchableOpacity>
            </View>
          </View>
        }

      </View>
    )
  }
  handleToggleSwitch = () => {
    if (this.state.toggleSwitch == false) {
      this.setState({ toggleSwitch: true })
    }
    if (this.state.toggleSwitch == true) {
      this.setState({ toggleSwitch: false })
    }
  }
  swiperImages = () => {
    return (
      <View style={styles.pagerViewMainView}>
        <PagerView style={{ flex: 1 }} initialPage={0}>
          <View style={styles.pagerViewSubView} key={1}>
            <Image source={add01} resizeMode='cover' style={styles.pagerViewImage} />
          </View>

          <View style={styles.pagerViewSubView} key={2}>
            <Image source={add02} resizeMode='cover' style={styles.pagerViewImage} />
          </View>

          <View style={styles.pagerViewSubView} key={3}>
            <Image source={add03} resizeMode='cover' style={styles.pagerViewImage} />
          </View>

          <View style={styles.pagerViewSubView} key={4}>
            <Image source={add04} resizeMode='cover' style={styles.pagerViewImage} />
          </View>
        </PagerView>
      </View>
    )
  }

  swiperImages02 = () => {
    return (
      <View style={styles.pagerViewMainView}>
        <PagerView style={{ flex: 1 }} initialPage={0}>
          <View style={styles.pagerViewSubView} key={1}>
            <Image source={add001} resizeMode='cover' style={styles.pagerViewImage} />
          </View>

          <View style={styles.pagerViewSubView} key={2}>
            <Image source={add002} resizeMode='cover' style={styles.pagerViewImage} />
          </View>

          <View style={styles.pagerViewSubView} key={3}>
            <Image source={add003} resizeMode='cover' style={styles.pagerViewImage} />
          </View>

          <View style={styles.pagerViewSubView} key={4}>
            <Image source={add004} resizeMode='cover' style={styles.pagerViewImage} />
          </View>
        </PagerView>
      </View>
    )
  }

  clipArtList = () => {
    return (

      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={styles.scrollviewMain}>
        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt01} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>Super Coins</Text>
        </View>

        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt02} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>Spoyl</Text>
        </View>

        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt03} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>Prepaid Recharge</Text>
        </View>

        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt04} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>Money+</Text>
        </View>

        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt05} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>NextGen Brands</Text>
        </View>

        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt06} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>LiveShop+</Text>
        </View>

        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt07} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>Game Zone</Text>
        </View>

        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt08} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>EMI</Text>
        </View>

        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt09} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>Wedding</Text>
        </View>

        <View style={styles.clipArtMainView}>
          <View style={styles.clipArtImageView}>
            <Image source={clipArt10} style={styles.clipArtImage} />
          </View>
          <Text style={styles.scrollViewText}>FireDrops</Text>
        </View>
      </ScrollView>
    )
  }
  topRatedItems = () => {
    return (
      <View style={{ marginHorizontal: 15 }}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.state.advertisement}
          renderItem={({ item }) => this.renderAdvertisement(item)}
        />
      </View>
    )
  }
  renderAdvertisement = (item) => {
    return (
      <View style={[styles.advertisementMainView, { borderWidth: 0.5, borderColor: "#f2f2f5" }]}>
        <View style={styles.advertisementSubView}>
          <View style={styles.imageAdvertisementView}>
            <Image resizeMode='cover' style={styles.imageAddvertisement} source={item.addimage} />
          </View>
          <Text style={styles.text01}>{item.text01}</Text>
          <Text style={[styles.text01, { fontWeight: '500' }]}>{item.text02}</Text>
        </View>
      </View>
    )

  }
  recentView = () => {
    return (
      <View style={styles.recentFlatListView}>
        <Text style={styles.recentText}>Recently Viwed Stors</Text>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.state.recentViewArray}
          renderItem={({ item }) => this.renderRecentItems(item)}
        />
      </View>
    )
  }
  renderRecentItems = (item) => {
    return (
      <View style={styles.recentMainView}>
        <Image source={item.recentAddImage} style={styles.recentImage} />
        <Text style={styles.text01}>{item.text}</Text>
      </View>
    )
  }
  sponsored = () => {
    return (
      <View style={styles.recentFlatListView}>
        <Text style={styles.recentText}>Sponsored</Text>
        <FlatList
          numColumns={3}
          data={this.state.sponsoredArray}
          renderItem={({ item }) => this.renderSponsored(item)}
        />
      </View>
    )
  }
  renderSponsored = (item) => {
    return (
      <View style={styles.renderSponsoredMainView}>
        <View style={[styles.advertisementSubView, { borderWidth: Scale(0.7), borderColor: "#7e807f" }]}>
          <View style={styles.imageAdvertisementView}>
            <Image resizeMode='cover' style={{ width: Scale(119), height: Scale(100) }} source={item.recentAddImage} />
          </View>
          <Text style={styles.text01}>{item.text}</Text>
          <Text style={[styles.text01, { fontWeight: '500' }]}>{item.price}</Text>
        </View>
      </View>
    )
  }

  suggestForYou = () => {
    return (
      <View>
        <View style={styles.suggestHeading}>
          <Text style={styles.recentText}>Suggested for You</Text>
          <TouchableOpacity style={styles.rightArrowBlueColor}>
            <Text style={styles.rightArrow}>&#62;</Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginHorizontal: Scale(10), marginVertical: Scale(10) }}>
          <FlatList
            numColumns={3}
            data={this.state.recentViewArray}
            renderItem={({ item }) => this.renderSuggestion(item)}
          />
        </View>
      </View>
    )
  }
  renderSuggestion = (item) => {
    return (
      <View style={styles.suggestionMainView}>
        <Image resizeMode='contain' source={item.recentAddImage} style={styles.suggestionImage} />
        <Text style={styles.text01}>{item.text}</Text>
        <View style={styles.suggestionSubView}>
          <Text style={[styles.text01, { textDecorationLine: 'line-through', textDecorationStyle: 'solid', color: '#808080', marginRight: Scale(5) }]}>{item.strickPrice}</Text>
          <Text style={styles.text01}>{item.price}</Text>
        </View>
        <Text style={[styles.text01, { color: 'green' }]}>{item.delivery}</Text>
      </View>
    )
  }

  bottomNavigation = () => {
    return (
      <View style={{ borderWidth: 0.5, justifyContent: 'space-between', flexDirection: 'row', height: Scale(70) }}>
        <FlatList
          horizontal={true}
          data={this.state.bottomNav}
          renderItem={({ item }) => this.renderBottomNavigation(item)}
        />
      </View>
    )
  }

  renderBottomNavigation = (item) => {
    return (

      <View style={{ marginHorizontal: Scale(17), justifyContent: 'center', alignItems: 'center' }}>
        <Image source={item.imageNav} resizeMode='cover' style={{ width: Scale(30), height: Scale(30), backgroundColor: '#fff' }} />
        <Text style={styles.text01}>{item.textNav}</Text>
      </View>
    )
  }
  render() {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
        {this.renderHeader()}
        {this.searchAndToggle()}
        {this.state.toggleSwitch ? this.swiperImages02() : this.swiperImages()}
        {this.clipArtList()}
        {this.topRatedItems()}
        {this.recentView()}
        {this.sponsored()}
        {this.suggestForYou()}
        <ListWithBackground/>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  filpkartImage: {
    height: Scale(25),
    width: Scale(25),
    resizeMode: "contain",
  },
  flipGrocImageView: {
    height: Scale(50),
    width: Scale(180),
    justifyContent: "center",
    alignItems: "center",
    flexDirection: 'row',
    borderRadius: Scale(10),
  },
  flipkartLogoText: {
    color: 'white',
    fontSize: Scale(12),
    fontWeight: '600',
    fontStyle: 'italic',
  },
  flipkartGroceryView: {
    height: Scale(50),
    width: Scale(180),
    justifyContent: "center",
    alignItems: "center",
    flexDirection: 'row',
    borderRadius: Scale(10),
  },
  groceryLogoText: {
    color: 'black',
    fontSize: Scale(12),
    fontWeight: '600',
    fontStyle: 'italic',

  },
  headerView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Scale(400),
    marginTop: Scale(10),
  },
  headerMainView: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginHorizontal: Scale(20),
  },
  toggleON: {
    backgroundColor: '#E6E6E9', width: Scale(50), height: Scale(18), borderRadius: Scale(30), flexDirection: "row-reverse", alignItems: 'center', paddingVertical: Scale(1), paddingHorizontal: Scale(2)
  },
  toggleOnBlank: {
    width: Scale(14), height: Scale(14), borderRadius: Scale(30), backgroundColor: "#fff"
  },
  toggleONText: {
    fontSize: Scale(10),
  },
  toggleOff: {
    backgroundColor: '#E6E6E9', width: Scale(50), height: Scale(18), borderRadius: Scale(30), flexDirection: "row", alignItems: 'center', paddingVertical: Scale(1), paddingHorizontal: Scale(2),
  },
  toggleOffBlank: {
    width: Scale(14), height: Scale(14), borderRadius: Scale(30), backgroundColor: "#fff"
  },
  toggleOffText: {
    fontSize: Scale(10),
  },
  brandMall: {
    fontSize: Scale(12),
    color: "#4a4a48",
    marginBottom: Scale(8),
  },
  searchAndSwitch: {
    marginTop: Scale(12),
    marginHorizontal: Scale(15),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  searchWithMikeAndCemera: {
    borderWidth: Scale(1),
    width: Scale(300),
    borderColor: '#9c9c98',
    borderRadius: Scale(10),
    paddingVertical: Scale(5),
    paddingHorizontal: Scale(5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  mikeStyle: { width: Scale(20), height: Scale(20) },
  cemeraStyle: { width: Scale(20), height: Scale(20), },
  TextInputStyle: {
    width: Scale(200),
    backgroundColor: '#fff',
  },
  searchIcon: {
    width: Scale(20), height: Scale(20)
  },
  pagerViewSubView: {
    justifyContent: 'center', alignItems: 'center'
  },
  pagerViewImage: {
    width: '100%', height: Scale(150),
  },
  pagerViewMainView:
    { height: Scale(150), marginTop: Scale(15) },
  clipArtImage: {
    width: Scale(50),
    height: Scale(50),
  },
  clipArtImageView: {
    width: Scale(70),
    height: Scale(70),
    backgroundColor: '#d2dafa',
    borderRadius: Scale(50),
    justifyContent: "center",
    alignItems: 'center',
  },
  clipArtMainView: {
    width: Scale(70),
    height: Scale(90),
    alignItems: 'center',
    margin: Scale(10)
  },
  scrollviewMain: {
    marginTop: Scale(10),
    height: Scale(130),
  },
  scrollViewText: {
    fontSize: Scale(10),
    fontWeight: '600',
    marginTop: Scale(5)
  },
  text01: { fontSize: Scale(11) },
  imageAddvertisement: { width: Scale(100), height: Scale(90) },
  imageAdvertisementView: { backgroundColor: '#d2dafa', height: Scale(100), width: Scale(110), justifyContent: 'center', alignItems: 'center' },
  advertisementSubView: { width: Scale(120), height: Scale(140), alignItems: 'center' },
  advertisementMainView: { padding: Scale(10) },
  recentMainView: { width: Scale(100), height: Scale(120), justifyContent: 'space-between', alignItems: 'center', borderWidth: Scale(1), borderColor: "#e1e3e1", marginRight: Scale(10), marginTop: Scale(5), },
  recentImage: { width: Scale(90), height: Scale(90), backgroundColor: '#fff' },
  recentText: {
    fontSize: Scale(18),
    fontWeight: "500",
    marginBottom: Scale(10),
  },
  recentFlatListView: {
    marginLeft: Scale(10),
    marginTop: Scale(10),
  },
  renderSponsoredMainView: {
    marginRight: Scale(20),
    marginTop: Scale(10)
  },
  rightArrowBlueColor: {
    width: Scale(30),
    height: Scale(30),
    backgroundColor: '#5b60f0',
    borderRadius: Scale(30),
    justifyContent: 'center',
    alignItems: 'center',
  },
  suggestHeading: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Scale(10),
    marginHorizontal: Scale(10),
  },
  rightArrow: {
    color: 'white',
    fontWeight: '600',
  },
  suggestionImage: {
    width: Scale(190),
    height: Scale(100),
  },
  suggestionMainView: { borderWidth: Scale(0.7), borderColor: "#7e807f", width: Scale(130), height: Scale(160), alignItems: 'center', marginRight: Scale(10) },
  suggestionSubView: { flexDirection: 'row', justifyContent: 'space-between' },
  listImage: {
    width: Scale(150),
    height: Scale(180),
    marginVertical: Scale(7),
  },
  special: {
    fontSize: 13,
    fontWeight: '600'
  },
  listBackground: {
    marginTop: Scale(10),
    marginBottom: Scale(20),
  },
  backGroundListMainView: { borderColor: "#7e807f", width: Scale(190), height: Scale(250), borderWidth: 0.7, backgroundColor: "#fff", marginHorizontal: Scale(10), alignItems: 'center', marginBottom: Scale(10), marginTop: Scale(10), borderRadius: 10, paddingVertical: Scale(5) }

})