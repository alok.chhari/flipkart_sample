import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, FlatList, ScrollView, ImageBackground, Modal, Button, Pressable, ToastAndroid } from 'react-native';
import { flipkartLogo, mike, cemera, flipsearchIcon, add01, add02, add03, add04, add001, add002, add003, add004, clipArt01, clipArt02, clipArt03, clipArt04, clipArt05, clipArt06, clipArt07, clipArt08, clipArt09, clipArt10, bgImage, addi01, addi02, addi03, addi04, plusFAssured, spoyl } from './assets'
import { AntDesign } from '@expo/vector-icons';
import { EvilIcons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import { FontAwesome6 } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { SimpleLineIcons } from '@expo/vector-icons';
import Scale from './Scale'
import { SafeAreaView } from 'react-native-safe-area-context';
import { Feather } from '@expo/vector-icons';
export default class AddidasTShirt extends Component {

    constructor() {
        super()
        this.state = {
            sortedBy: false,
            sortedByBooleanState: {
                relevance: false,
                popularity: false,
                lowtohigh: false,
                hightolow: false,
                newest: false,
            },
            searchModal: false,
            textInputFilledValue: '',
            searchItems: [
                {
                    searchImage: require("../assets/coffee01.png"),
                    searchText: "Coffee01",
                },
                {
                    searchImage: require("../assets/coffee02.png"),
                    searchText: "Coffee02",
                }, {
                    searchImage: require("../assets/coffeebag.png"),
                    searchText: "Coffeebag",
                }, {
                    searchImage: require("../assets/coffeeBg.png"),
                    searchText: "CoffeeBg",
                }, {
                    searchImage: require("../assets/coffeeWithPlat.png"),
                    searchText: "CoffeePlat",
                }, {
                    searchImage: require("../assets/cup.png"),
                    searchText: "cup",
                }, {
                    searchImage: require("../assets/earbuds.png"),
                    searchText: "earbuds",
                }, {
                    searchImage: require("../assets/earbuds01.png"),
                    searchText: "earbuds01",
                }, {
                    searchImage: require("../assets/fashion01.png"),
                    searchText: "fashion01",
                }, {
                    searchImage: require("../assets/filter.png"),
                    searchText: "filter",
                }, {
                    searchImage: require("../assets/flipkartLogo.png"),
                    searchText: "flipkartLogo",
                }, {
                    searchImage: require("../assets/heart.png"),
                    searchText: "heart",
                }, {
                    searchImage: require("../assets/home.png"),
                    searchText: "home",
                }, {
                    searchImage: require("../assets/homeIcon.png"),
                    searchText: "homeIcon",
                }, {
                    searchImage: require("../assets/leftArrow.png"),
                    searchText: "leftArrow",
                }, {
                    searchImage: require("../assets/Line.png"),
                    searchText: "line",
                }, {
                    searchImage: require("../assets/mike.png"),
                    searchText: "mike",
                }, {
                    searchImage: require("../assets/moto.png"),
                    searchText: "moto",
                }, {
                    searchImage: require("../assets/add001.jpg"),
                    searchText: "add001",
                }, {
                    searchImage: require("../assets/add01.png"),
                    searchText: "add01",
                }, {
                    searchImage: require("../assets/add02.png"),
                    searchText: "add02",
                }, {
                    searchImage: require("../assets/add002.png"),
                    searchText: "add002",
                }, {
                    searchImage: require("../assets/add03.jpg"),
                    searchText: "add03",
                }, {
                    searchImage: require("../assets/add003.jpg"),
                    searchText: "add003",
                }, {
                    searchImage: require("../assets/add004.jpg"),
                    searchText: "add004",
                }, {
                    searchImage: require("../assets/add04.png"),
                    searchText: "add004",
                }, {
                    searchImage: require("../assets/addi05.jpg"),
                    searchText: "addi05",
                }, {
                    searchImage: require("../assets/addi06.jpg"),
                    searchText: "addi06",
                }, {
                    searchImage: require("../assets/addi07.jpg"),
                    searchText: "addi07",
                }, {
                    searchImage: require("../assets/addi08.jpg"),
                    searchText: "addi08",
                }, {
                    searchImage: require("../assets/addiBlack.png"),
                    searchText: "addiblack",
                }, {
                    searchImage: require("../assets/addigray.png"),
                    searchText: "addigray",
                }, {
                    searchImage: require("../assets/airlines01.png"),
                    searchText: "airlines01",
                }, {
                    searchImage: require("../assets/bata.png"),
                    searchText: "bata",
                }, {
                    searchImage: require("../assets/bell.png"),
                    searchText: "bell",
                },
            ],
            bullies: [{
                textSort: 'Popular Brands',
                imageSort: require('../assets/clipArt01.png')
            },
            {
                textSort: 'Spoyl',
                imageSort: require('../assets/clipArt02.png')
            },
            {
                textSort: 'Top Rated',
                imageSort: require('../assets/clipArt03.png')
            }, {
                textSort: 'New Arrivals',
                imageSort: require('../assets/clipArt04.png')
            }],
            recentViewArray: [{
                recentAddImage: require('../assets/trimmer.png'),
                text: "trimmer",
                price: '$1200',
                delivery: 'Free Delivery',
                strickPrice: "$3200",
            },
            {
                recentAddImage: require('../assets/moto.png'),
                text: "Motorola",
                price: '$1300',
                delivery: 'Free Delivery',
                strickPrice: "$2200",
            },
            {
                recentAddImage: require('../assets/bata.png'),
                text: "Bata Shoes",
                price: '$1400',
                delivery: 'Free Delivery',
                strickPrice: "$1900",
            },
            {
                recentAddImage: require('../assets/earbuds.png'),
                text: "Sony Earbuds",
                price: '$1500',
                delivery: 'Free Delivery',
                strickPrice: "$1800",
            },
            {
                recentAddImage: require('../assets/tshirt.png'),
                text: "Addidas T-Shirt",
                price: '$1600',
                delivery: 'Free Delivery',
                strickPrice: "$1700",
            },
            {
                recentAddImage: require('../assets/smartWatch.png'),
                text: "Amaze Fit Smart",
                price: '$1700',
                delivery: 'Free Delivery',
                strickPrice: "$1600",
            },
            {
                recentAddImage: require('../assets/socks.png'),
                text: "Rebook Socks",
                price: '$1800',
                delivery: 'Free Delivery',
                strickPrice: "$1500",
            },
            {
                recentAddImage: require('../assets/vest.png'),
                text: "Jockey Vest",
                price: '$1900',
                delivery: 'Free Delivery',
                strickPrice: "$1400",
            },
            {
                recentAddImage: require('../assets/toy.png'),
                text: "Toys",
                price: '$2200',
                delivery: 'Free Delivery',
                strickPrice: "$1300",
            },
            {
                recentAddImage: require('../assets/cap.png'),
                text: "Under Armar Cap",
                price: '$3200',
                delivery: 'Free Delivery',
                strickPrice: "$1200",
            }],
        }
    }
    renderHeader = () => {
        return (
            <View>
                <View style={styles.headerView}>
                    <TouchableOpacity>
                        <AntDesign name="arrowleft" size={24} color="black" />
                    </TouchableOpacity>
                    <View style={styles.searchWithMikeAndCemera}>
                        <TouchableOpacity><Image source={flipsearchIcon} style={styles.searchIcon} /></TouchableOpacity>
                        <TextInput style={styles.TextInputStyle} placeholder='Addidas T-Shirt' onPressIn={() => this.setState(prevState => ({ searchModal: !prevState.searchModal }))} />
                        <TouchableOpacity><Image source={mike} style={styles.mikeStyle} /></TouchableOpacity>
                        <TouchableOpacity><Image source={cemera} style={styles.cemeraStyle} /></TouchableOpacity>
                    </View>
                    <TouchableOpacity>
                        <EvilIcons name="cart" size={34} color="black" />
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType='slide'
                    visible={this.state.searchModal}
                >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => this.setState(prevState => ({ searchModal: !prevState.searchModal, textInputFilledValue: "" }))} >
                            <AntDesign name="arrowleft" size={24} color="black" />
                        </TouchableOpacity>
                        <View style={[styles.searchWithMikeAndCemera, { alignSelf: 'center' }]}>
                            <TouchableOpacity><Image source={flipsearchIcon} style={styles.searchIcon} /></TouchableOpacity>
                            <TextInput style={{ height: Scale(40), width: Scale(250) }} value={this.state.textInputFilledValue} onChangeText={(text) => this.setState({ textInputFilledValue: text })} placeholder='Search ...' />
                            <TouchableOpacity><Image source={cemera} style={styles.cemeraStyle} /></TouchableOpacity>
                        </View>
                        <TouchableOpacity>
                            <EvilIcons name="cart" size={34} color="black" />
                        </TouchableOpacity>
                    </View>
                    <FlatList
                        data={this.state.searchItems}
                        renderItem={({ item }) => this.handleSearch(item)}
                    />
                </Modal>
            </View>
        )
    }

    handleSearch = (item) => {
        if (this.state.textInputFilledValue === "") {
            return (
                <View style={{ borderWidth: 0.5, borderColor: '#9c9c9c', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                    <Image source={item.searchImage} resizeMode='contain' style={{ width: Scale(40), height: Scale(40), borderRadius: Scale(200) }} />
                    <Text>{item.searchText}</Text>
                    <Feather name="arrow-up-left" size={24} color="black" onPress={() => this.setState({ textInputFilledValue: item.searchText })} />
                </View>
            )
        }

        if (item.searchText.toLowerCase().includes(this.state.textInputFilledValue.toLowerCase())) {
            return (
                <View style={{ borderWidth: 0.5, borderColor: '#9c9c9c', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                    <Image source={item.searchImage} resizeMode='contain' style={{ width: Scale(40), height: Scale(40), borderRadius: Scale(200) }} />
                    <Text>{item.searchText}</Text>
                    <Feather name="arrow-up-left" size={24} color="black" onPress={() => this.setState({ textInputFilledValue: item.searchText })} />
                </View>
                )
        }
    }
    handleSortedby = () => {
        this.setState(prevState => ({ sortedBy: !prevState.sortedBy }))
        this.setState(prevState => ({ sortedByBooleanState: { ...prevState.sortedByBooleanState, lowtohigh: false ,relevance: false,popularity: false,hightolow: false,newest: false }}))
    }

    showToastMessage = () => {
        ToastAndroid.show(
            'Filter Methods',
            ToastAndroid.CENTER,
        )
    }
    renderSortify = () => {
        return (
            <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} style={styles.sortifyScrollView}>
                <TouchableOpacity style={styles.sortifySubView} onPress={this.handleSortedby}>
                    <Text style={styles.sortifySubSubView}>Sort By</Text>
                    <SimpleLineIcons name="arrow-down" size={10} style={{ fontWeight: 'bold' }} color="black" />
                </TouchableOpacity>

                <TouchableOpacity style={styles.sortifySubView} onPress={() => this.showToastMessage()}>
                    <Text style={styles.sortifySubSubView}>&#8644;</Text>
                    <Text>Filter</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.sortifySubView}>
                    <Text style={styles.sortifySubSubView}>Size</Text>
                    <SimpleLineIcons name="arrow-down" size={10} style={{ fontWeight: 'bold' }} color="black" />
                </TouchableOpacity>

                <TouchableOpacity style={styles.sortifySubView}>
                    <Text style={styles.sortifySubSubView}>Color</Text>
                    <SimpleLineIcons name="arrow-down" size={10} style={{ fontWeight: 'bold' }} color="black" />
                </TouchableOpacity>

                <TouchableOpacity style={styles.sortifySubView}>
                    <Text style={styles.sortifySubSubView}>Pattern</Text>
                    <SimpleLineIcons name="arrow-down" size={10} style={{ fontWeight: 'bold' }} color="black" />
                </TouchableOpacity>

                <TouchableOpacity style={styles.sortifySubView}>
                    <Text style={styles.sortifySubSubView}>Price</Text>
                    <SimpleLineIcons name="arrow-down" size={10} style={{ fontWeight: 'bold' }} color="black" />
                </TouchableOpacity>

                <TouchableOpacity style={styles.sortifySubView}>
                    <Text style={styles.sortifySubSubView}>Customer Ratting</Text>
                    <SimpleLineIcons name="arrow-down" size={10} style={{ fontWeight: 'bold' }} color="black" />
                </TouchableOpacity>
            </ScrollView>
        )
    }

    renderBullies = () => {
        return (
            <View style={{ marginTop: Scale(10) }}>
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    data={this.state.bullies}
                    renderItem={({ item }) => this.handleBullies(item)}
                />
            </View>
        )
    }

    handleBullies = (item) => {
        return (
            <TouchableOpacity style={styles.bulliesView}>
                <Image source={item.imageSort} resizeMode='contain' style={styles.bulliesImage} />
                <View style={styles.bulliesSubView}>
                    <Text style={styles.bulliesText}>{item.textSort}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    bestSeller = () => {
        return (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                <View style={styles.bestSellerMainView}>
                    <ImageBackground source={addi01} resizeMode='cover' style={styles.bestSellerBackGroundImage} />
                    <View style={styles.detailsPrice}>
                        <Text>Surhi</Text>
                        <View style={styles.bestSellerPriceWar}>
                            <Text style={styles.bestSellerPricePercentage}>&#8595;25%</Text>
                            <Text style={styles.bestSellerPriceStrice}>$2200</Text>
                            <Text style={styles.bestSellerOriginalPrice}>2000</Text>
                        </View>
                        <Text style={styles.bestSellerBank}>Bank Offer</Text>
                        <View style={styles.bestSellerratingMain}>
                            <Text style={styles.bestSellerStars}>&#9733;&#9733;&#9733;&#9733;</Text>
                            <Text>&#40;15,550&#41;</Text>
                            <Image source={plusFAssured} style={styles.bestSellerPlus} />
                        </View>
                        <Text>Free Delivery</Text>
                    </View>
                </View>

                <View style={styles.bestSellerMainView}>
                    <ImageBackground source={addi02} resizeMode='cover' style={styles.bestSellerBackGroundImage} />
                    <View style={styles.detailsPrice}>
                        <Text>Addidas</Text>
                        <View style={styles.bestSellerPriceWar}>
                            <Text style={styles.bestSellerPricePercentage}>&#8595;35%</Text>
                            <Text style={styles.bestSellerPriceStrice}>$3200</Text>
                            <Text style={styles.bestSellerOriginalPrice}>2000</Text>
                        </View>
                        <Text style={styles.bestSellerBank}>Bank Offer</Text>
                        <View style={styles.bestSellerratingMain}>
                            <Text style={styles.bestSellerStars}>&#9733;&#9733;</Text>
                            <Text>&#40;45,550&#41;</Text>
                            <Image source={plusFAssured} style={styles.bestSellerPlus} />
                        </View>
                        <Text>Free Delivery</Text>
                    </View>
                </View>

                <View style={styles.bestSellerMainView}>
                    <ImageBackground source={addi03} resizeMode='cover' style={styles.bestSellerBackGroundImage} />
                    <View style={styles.detailsPrice}>
                        <Text>Armani</Text>
                        <View style={styles.bestSellerPriceWar}>
                            <Text style={styles.bestSellerPricePercentage}>&#8595;55%</Text>
                            <Text style={styles.bestSellerPriceStrice}>$1700</Text>
                            <Text style={styles.bestSellerOriginalPrice}>1000</Text>
                        </View>
                        <Text style={styles.bestSellerBank}>Bank Offer</Text>
                        <View style={styles.bestSellerratingMain}>
                            <Text style={styles.bestSellerStars}>&#9733;&#9733;&#9733;&#9733;</Text>
                            <Text>&#40;19,550&#41;</Text>
                            <Image source={plusFAssured} style={styles.bestSellerPlus} />
                        </View>
                        <Text>Free Delivery</Text>
                    </View>
                </View>

                <View style={styles.bestSellerMainView}>
                    <ImageBackground source={addi04} resizeMode='cover' style={styles.bestSellerBackGroundImage} />
                    <View style={styles.detailsPrice}>
                        <Text>Skecher</Text>
                        <View style={styles.bestSellerPriceWar}>
                            <Text style={styles.bestSellerPricePercentage}>&#8595;95%</Text>
                            <Text style={styles.bestSellerPriceStrice}>$2200</Text>
                            <Text style={styles.bestSellerOriginalPrice}>2000</Text>
                        </View>
                        <Text style={styles.bestSellerBank}>Bank Offer</Text>
                        <View style={styles.bestSellerratingMain}>
                            <Text style={styles.bestSellerStars}>&#9733;&#9733;&#9733;</Text>
                            <Text>&#40;18,550&#41;</Text>
                            <Image source={plusFAssured} style={styles.bestSellerPlus} />
                        </View>
                        <Text>Free Delivery</Text>
                    </View>
                </View>

            </View>
        )
    }
    latestStyle = () => {
        return (
            <View>
                <LinearGradient colors={['#b3eafc', '#e9bbfa', '#fabbbe']}>
                    <View style={styles.suggestHeading}>
                        <View>
                            <Text style={styles.recentText}>Latest Style for You</Text>
                            <Image source={spoyl} resizeMode='contain' style={{ width: Scale(50), height: Scale(15), marginTop: Scale(-10) }} />
                        </View>
                        <EvilIcons name="arrow-right" size={34} color="black" />
                    </View>
                    <View style={{ marginHorizontal: Scale(10), marginVertical: Scale(10) }}>
                        <FlatList
                            horizontal={true}
                            data={this.state.recentViewArray}
                            renderItem={({ item }) => this.renderSuggestion(item)}
                        />
                    </View>

                </LinearGradient>
            </View>
        )
    }
    renderSuggestion = (item) => {
        return (
            <View style={styles.suggestionMainView}>
                <Image resizeMode='contain' source={item.recentAddImage} style={styles.suggestionImage} />
                <Text style={styles.text01}>{item.text}</Text>
                <View style={styles.suggestionSubView}>
                    <Text style={[styles.text01, { textDecorationLine: 'line-through', textDecorationStyle: 'solid', color: '#808080', marginRight: Scale(5) }]}>{item.strickPrice}</Text>
                    <Text style={styles.text01}>{item.price}</Text>
                </View>
                <Text style={[styles.text01, { color: 'green' }]}>{item.delivery}</Text>
            </View>
        )
    }
    Relevance = () => {
        this.setState(prevState => ({ sortedByBooleanState: { ...prevState.sortedByBooleanState, relevance: !prevState.sortedByBooleanState.relevance ,popularity: false,lowtohigh: false,hightolow: false,newest: false }}))
    }

    Popularity = () => {
        this.setState(prevState => ({ sortedByBooleanState: { ...prevState.sortedByBooleanState, popularity: !prevState.sortedByBooleanState.popularity,relevance: false ,lowtohigh: false , hightolow: false, newest: false} }))
    }

    Lowtohigh = () => {
        this.setState(prevState => ({ sortedByBooleanState: { ...prevState.sortedByBooleanState, lowtohigh: !prevState.sortedByBooleanState.lowtohigh,relevance: false ,popularity: false,hightolow: false,newest: false } }))
    }

    Hightolow = () => {
        this.setState(prevState => ({ sortedByBooleanState: { ...prevState.sortedByBooleanState, hightolow: !prevState.sortedByBooleanState.hightolow ,relevance: false ,lowtohigh: false,newest: false,popularity: false} }))
    }

    Newest = () => {
        this.setState(prevState => ({ sortedByBooleanState: { ...prevState.sortedByBooleanState, newest: !prevState.sortedByBooleanState.newest ,relevance: false,lowtohigh: false,hightolow: false,popularity: false} }))
    }
    render() {
        return (
            <SafeAreaView>
                {this.renderHeader()}
                {this.renderSortify()}
                <ScrollView style={{ marginBottom: 'auto' }}>
                    {this.renderBullies()}
                    {this.bestSeller()}
                    {this.latestStyle()}
                    {this.bestSeller()}
                    <Modal transparent={true} animationType="slide" visible={this.state.sortedBy}>
                        <Pressable onPress={this.handleSortedby} style={{ flex: 1.5, borderWidth: 1, backgroundColor: "#141414", opacity: 0.5, justifyContent: 'center', alignItems: 'center' }}>
                        </Pressable>
                        <View style={{ flex: 0.7, backgroundColor: "#fff" }}>

                            <View style={{ borderBottom: 100, borderBottomWidth: 1, borderColor: "#919191", padding: Scale(15) }}>
                                <Text style={{ fontSize: 18, fontWeight: '600', color: "#919191" }}>Sorted By</Text>
                            </View>
                            <View style={{ padding: Scale(10) }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: Scale(10) }}>
                                    <Text style={{ fontSize: 16, fontWeight: '600' }}>Relevance</Text>
                                    <TouchableOpacity style={{ width: Scale(20), height: Scale(20), borderWidth: Scale(1), borderRadius: Scale(50), justifyContent: 'center', alignItems: 'center', borderColor: this.state.sortedByBooleanState.relevance ? "blue" : "black" }} onPress={this.Relevance}>{this.state.sortedByBooleanState.relevance ? <FontAwesome name="circle" size={14} color="blue" /> : ""}</TouchableOpacity>
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: Scale(10) }}>
                                    <Text style={{ fontSize: 16, fontWeight: '600' }}>Popularity</Text>
                                    <TouchableOpacity style={{ width: Scale(20), height: Scale(20), borderWidth: Scale(1), borderRadius: Scale(50), justifyContent: 'center', alignItems: 'center', borderColor: this.state.sortedByBooleanState.popularity ? "blue" : "black" }} onPress={this.Popularity}>{this.state.sortedByBooleanState.popularity ? <FontAwesome name="circle" size={14} color="blue" /> : ""}</TouchableOpacity>
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: Scale(10) }}>
                                    <Text style={{ fontSize: 16, fontWeight: '600' }}>Price -- Low to high</Text>
                                    <TouchableOpacity style={{ width: Scale(20), height: Scale(20), borderWidth: Scale(1), borderRadius: Scale(50), justifyContent: 'center', alignItems: 'center', borderColor: this.state.sortedByBooleanState.lowtohigh ? "blue" : "black" }} onPress={this.Lowtohigh}>{this.state.sortedByBooleanState.lowtohigh ? <FontAwesome name="circle" size={14} color="blue" /> : ""}</TouchableOpacity>
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: Scale(10) }}>
                                    <Text style={{ fontSize: 16, fontWeight: '600' }}>Price -- High to low</Text>
                                    <TouchableOpacity style={{ width: Scale(20), height: Scale(20), borderWidth: Scale(1), borderRadius: Scale(50), justifyContent: 'center', alignItems: 'center', borderColor: this.state.sortedByBooleanState.hightolow ? "blue" : "black" }} onPress={this.Hightolow}>{this.state.sortedByBooleanState.hightolow ? <FontAwesome name="circle" size={14} color="blue" /> : ""}</TouchableOpacity>
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontSize: 16, fontWeight: '600' }}>Newest First</Text>
                                    <TouchableOpacity style={{ width: Scale(20), height: Scale(20), borderWidth: Scale(1), borderRadius: Scale(50), justifyContent: 'center', alignItems: 'center', borderColor: this.state.sortedByBooleanState.newest ? "blue" : "black" }} onPress={this.Newest}>{this.state.sortedByBooleanState.newest ? <FontAwesome name="circle" size={14} color="blue" /> : ""}</TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </Modal>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    searchAndSwitch: {
        marginTop: Scale(12),
        marginHorizontal: Scale(15),
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    searchWithMikeAndCemera: {
        borderWidth: Scale(1),
        width: Scale(300),
        borderColor: '#9c9c98',
        borderRadius: Scale(10),
        paddingVertical: Scale(5),
        paddingHorizontal: Scale(5),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    mikeStyle: { width: Scale(20), height: Scale(20) },
    cemeraStyle: { width: Scale(20), height: Scale(20), },
    TextInputStyle: {
        width: Scale(200),
        backgroundColor: '#fff',
    },
    searchIcon: {
        width: Scale(20), height: Scale(20)
    },
    headerView: {
        marginHorizontal: Scale(10),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    sortifyScrollView: {
        marginTop: Scale(10),
    },
    sortifySubView: { borderWidth: Scale(1), height: Scale(40), padding: Scale(6), borderRadius: Scale(25), borderColor: "#999999", marginHorizontal: Scale(5), marginVertical: Scale(10), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    sortifySubSubView: { marginRight: Scale(8) },
    bulliesView: { borderWidth: Scale(1), padding: Scale(3), borderRadius: Scale(5), borderColor: "#999999", flexDirection: 'row', alignItems: "center", width: Scale(100), marginHorizontal: Scale(5), marginBottom: Scale(10) },
    bulliesImage: { width: Scale(30), height: Scale(30), marginRight: Scale(8) },
    bulliesSubView: { width: Scale(53) },
    bulliesText: { fontSize: Scale(13) },
    bestSellerMainView: { width: Scale(210) },
    bestSellerBackGroundImage: { width: Scale(210), height: Scale(300) },
    bestSellerPriceWar: { flexDirection: 'row' },
    bestSellerPricePercentage: { fontWeight: '600', color: 'green', marginRight: Scale(5) },
    bestSellerPriceStrice: { textDecorationLine: 'line-through', fontWeight: '600', marginRight: Scale(5) },
    bestSellerOriginalPrice: { fontWeight: '600' },
    bestSellerBank: { fontSize: Scale(12), color: 'green', fontWeight: '600' },
    bestSellerratingMain: { flexDirection: 'row' },
    bestSellerStars: { color: 'green' },
    bestSellerPlus: { width: Scale(70), height: Scale(30) },
    suggestHeading: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: Scale(10),
        marginHorizontal: Scale(10),
    },
    recentText: {
        fontSize: Scale(18),
        fontWeight: "500",
        marginBottom: Scale(10),
    },
    rightArrowBlueColor: {
        width: Scale(30),
        height: Scale(30),
        backgroundColor: '#5b60f0',
        borderRadius: Scale(30),
        justifyContent: 'center',
        alignItems: 'center',
    },
    suggestionMainView: { borderWidth: Scale(0.7), borderColor: "#7e807f", width: Scale(130), height: Scale(160), alignItems: 'center', marginRight: Scale(10), backgroundColor: '#fff' },
    suggestionImage: {
        width: Scale(190),
        height: Scale(100),
    },
    text01: { fontSize: Scale(11) },
    suggestionSubView: { flexDirection: 'row', justifyContent: 'space-between' },
    detailsPrice: {
        marginLeft: Scale(15),
    }
})