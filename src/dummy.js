import React, { Component } from 'react';
import { SafeAreaView, View, Text, Image, TextInput, ScrollView, StyleSheet, FlatList, Modal, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import { coffeeWithPlat, leftArrow, emptyHeart, star ,line} from './assets';
import Scale from './Scale';
export default class CoffeeDetails extends Component {
    renderHeader = () => {
        return (
            <View>
                <ImageBackground source={coffeeWithPlat} resizeMode='cover' style={styles.bgImage}>
                    <View style={styles.leftArrowAndEmptyHeart}>
                        <View style={styles.whiteBG}>
                            <Image source={leftArrow} style={styles.headerImage} />
                        </View>
                        <View style={styles.whiteBG}>
                            <Image source={emptyHeart} style={styles.headerImage} />
                        </View>
                    </View>

                    <View style={styles.coffeeWithSugarMainView}>
                        <View style={styles.coffeeWithRating}>
                            <View style={styles.coffeeWithSugar}>
                                <Text style={styles.coffeeName}>Cappuccino</Text>
                                <Text style={styles.withSugar}>With Sugar</Text>
                            </View>
                            <View style={styles.starRating}>
                                <Image source={star} style={styles.starWH} />
                                <Text style={styles.rating}>4.5</Text>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        )
    }

    renderDetails = () => {
        return (
            <View style={styles.addtocartdetails}>
                <View>
                    <Text style={styles.cupSize}>Cup Size</Text>
                </View>
                <View style={styles.SML}>
                    <View style={styles.smallCup}>
                        <Text style={styles.small}>Small</Text>
                    </View>
                    <Text style={styles.medium}>Medium</Text>
                    <Text style={styles.medium}>Large</Text>
                </View>

                <View>
                    <Text style={styles.cupSize}>Sugar Level</Text>
                </View>
                <View style={styles.SML}>
                    <View style={styles.smallCup}>
                        <Text style={styles.small}>No Sugar</Text>
                    </View>
                    <Text style={styles.medium}>Low</Text>
                    <Text style={styles.medium}>Medium</Text>
                </View>

                <View>
                    <Text style={styles.cupSize}>About</Text>
                </View>
                <View style={styles.SML}>
                    <Text style={styles.loremText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec felis velit, commodo eget ultricies sed, fermentum ut sapien. Duis non nisl nibh. Maecenas dictum eget sapien id consectetur.</Text>
                </View>
                <View>
                    <View style={styles.priceAndAddtoCart}>
                        <Text style={styles.addtoCart}>Add to Cart</Text>
                        <Image source={line} style={styles.vLine}/>
                        <Text style={styles.addtoCart}>50.000</Text>
                    </View>
                </View>
            </View>
        )
    }
    render = () => {
        return (
            <ScrollView style={{ flex: 1 }}>
                {this.renderHeader()}
                {this.renderDetails()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    coffeePlat: {
        // flex:1,
    },
    bgImage: {
        height: 619,
        // flex: 1,
    },
    leftArrowAndEmptyHeart: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 33,
        marginTop: 33,
    },
    whiteBG: {
        backgroundColor: 'white',
        width: 33,
        height: 33,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerImage: {
        width: 26,
        height: 26,
    },
    coffeeName: {
        color: 'white',
        fontSize: 36,
        fontWeight: '600',
    },
    withSugar: {
        fontSize: 14,
        fontWeight: '500',
        color: 'white',
    },
    // coffeeWithSugar: {
    //     marginHorizontal: 33,
    //     marginBottom: 170,
    // },
    coffeeWithSugarMainView: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    starWH: {
        width: 20,
        height: 20,
        marginRight: 9,
    },
    starRating: {
        width: 77,
        height: 32,
        backgroundColor: '#C1925B',
        borderRadius: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
    },
    rating: {
        color: 'white',
        fontSize: 14,
        fontWeight: '500',
    },
    coffeeWithRating: {
        marginHorizontal: 33,
        marginBottom: 160,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    addtocartdetails: {
        flex: 1, marginTop: -150, borderTopLeftRadius: 30, borderTopRightRadius: 30, backgroundColor: 'white'
    },
    cupSize: {
        fontSize: 18,
        fontWeight: '600',
        marginTop: 24,
        marginHorizontal: 34,
    },
    small: {
        fontSize: 18,
        fontWeight: '500',
        color: 'white'
    },
    medium: {
        fontSize: 18,
        fontWeight: '500',
    },
    smallCup: {
        width: Scale(103),
        height: Scale(32),
        backgroundColor: '#00512C',
        justifyContent: 'center',
        flexDirection: "row",
        borderRadius: 30,
    },
    SML: {
        marginHorizontal: 34,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 14,
    },
    loremText: {
        fontSize: 12,
        fontWeight: '600',
    },
    priceAndAddtoCart:{
        backgroundColor:'#00512C',
        marginHorizontal:34,
        borderRadius:30,
        flexDirection:'row',
        justifyContent:"space-evenly",
        alignItems:"center",
        height:76,
        marginTop: 14,
        marginBottom:14,
    },
    addtoCart:{
        fontSize:20,
        fontFamily:'600',
        color:'white',
    },
    vLine:{
        height:30,
        borderRadius:20,
        width:5,
    }
})