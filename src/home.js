import React, { Component } from 'react';
import { View, Text, ImageBackground, SafeAreaView, Image, StyleSheet, Button, TouchableOpacity, Modal, TouchableWithoutFeedback } from 'react-native';
import { coffeeBack, coffeebag, dot, getstartbutton, bhoot } from './assets'
import Scale from './Scale';
export default class Home extends Component {
  constructor() {
    super()
    this.state = {
      isModalVisible: false,
    }
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground source={coffeeBack} style={{ flex: 1 }}>
          <SafeAreaView>
            <View style={{ height: 141 }}>

            </View>
            <View>
              <Image source={coffeebag} />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: "center" }}>
              <View style={{ marginTop: 20, width: 200, height: 100 }}>
                <Text style={{ color: 'white', fontSize: 24, fontWeight: '600', textAlign: 'center' }}>Coffee so good your taste buds will it.</Text>
              </View>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: "center" }}>
              <View style={{ marginTop: 20, width: 300, height: 40 }}>
                <Text style={{ color: 'white', fontSize: 14, fontWeight: '600', textAlign: 'center' }}>The best Gain, the  finest roas, the most powerfull flavor.</Text>
              </View>
            </View>

            <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: "center" }}>
              <View style={{ width: 300, flexDirection: 'row', justifyContent: "center" }}>
                <Image source={dot} />
              </View>
            </View>

            <TouchableOpacity onPress={()=>{this.setState({isModalVisible:true})}} style={{marginTop:20,flexDirection:'row',justifyContent:"center"}}>
              <Image source={getstartbutton} />
            </TouchableOpacity>

            <TouchableWithoutFeedback>
              <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.isModalVisible}
                onRequestClose={() => this.setState({ isModalVisible: false })}
                presentationStyle="formSheet"
              >
                <ImageBackground source={bhoot} resizeMode='cover' style={styles.modal}>
                  <Button title="Close Modal" onPress={()=>{this.setState({isModalVisible:!this.state.isModalVisible})}}/>
                </ImageBackground>
              </Modal>
            </TouchableWithoutFeedback>
          </SafeAreaView>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 300,
    width: Scale(350),
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    marginTop: 80,
    marginLeft: 40,
  },
})